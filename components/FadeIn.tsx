import React, { FunctionComponent, ReactNode, useEffect, useState } from "react";

interface Props {
    delay: number;
    children: ReactNode;
    disabled?: boolean;
    [key: string]: any;
}
const FadeIn: FunctionComponent<Props> = (props: Props) => {
    const [visible, setVisible] = useState(!!props.disabled);
    useEffect(() => {
        setTimeout(() => {
            setVisible(true);
        }, props.delay);
    }, []);
    return (
        <div className={`fade-in ${visible && "visible"}`} {...props}>
            {props.children}
        </div>
    );
};

export default FadeIn;
