import React, { FunctionComponent, useRef, useState } from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import OpenStreetMap from "../api-layer/OpenStreetMap";
import { debounce } from "lodash";
import { OpenStreetMapResponse } from "../shared";

interface Props {
    className?: string;
    onSelect: (openStreetMapResponse: OpenStreetMapResponse) => void;
    selected?: OpenStreetMapResponse;
}

const AddressInput: FunctionComponent<Props> = (props: Props) => {
    const [options, setOptions] = useState<OpenStreetMapResponse[]>([]);

    const query = useRef<(q) => void>(
        debounce(async q => {
            const res = await OpenStreetMap.search(q);
            setOptions(res);
        }, 1000),
    ).current;

    const handleChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
        const q = e.target.value;
        if (q.length > 5) {
            query(q);
        }
    };

    const handleSelect = (e, value) => {
        props.onSelect(value);
    };

    return (
        <Autocomplete
            options={options}
            freeSolo={true}
            className={props.className}
            onChange={handleSelect}
            filterOptions={o => o}
            getOptionLabel={option => option.display_name}
            renderInput={params => (
                <TextField
                    {...params}
                    label="Adresse"
                    error={!props.selected}
                    variant="outlined"
                    onChange={handleChange}
                    helperText={props.selected ? `Ausgewählt: ${props.selected.display_name}` : undefined}
                    inputProps={{ ...params.inputProps, autoComplete: "new-password" }}
                />
            )}
        />
    );
};

export default AddressInput;
