import React, { FunctionComponent, ReactNode } from "react";

interface Props {
    onSubmit: () => void;
    children: ReactNode;
    [key: string]: any;
}
const Form: FunctionComponent<Props> = (props: Props) => {
    const handleSubmit = e => {
        e.preventDefault();
        props.onSubmit();
    };
    return (
        <form {...props} onSubmit={handleSubmit}>
            {props.children}
        </form>
    );
};

export default Form;
