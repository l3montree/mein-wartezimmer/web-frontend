import React, { useContext } from "react";
import { AppContext } from "../context/AppContext";
import { Button, List } from "@material-ui/core";
import LocalTicketItem from "./LocalTicketItem";
import { useRouter } from "next/router";

const TicketList = () => {
    const router = useRouter();
    const { tickets } = useContext(AppContext);
    return (
        <>
            {tickets.length > 0 ? (
                <List className={"mb-4"}>
                    {tickets.map(ticket => (
                        <LocalTicketItem key={ticket.id} {...ticket} />
                    ))}
                </List>
            ) : (
                <>
                    <p className={"px-3"}>Keine Wartenummern gespeichert</p>
                </>
            )}
            <Button
                onClick={() => router.push("/organizations")}
                disableElevation={true}
                variant={"contained"}
                className={"mx-3"}
                color={"primary"}>
                Nummer ziehen
            </Button>
        </>
    );
};

export default TicketList;
