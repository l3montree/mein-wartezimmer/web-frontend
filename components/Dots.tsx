import React, { FunctionComponent } from "react";

interface Props {
    amount: number;
    active: number;
    className: string;
}
const Dots: FunctionComponent<Props> = (props: Props) => {
    return (
        <div className={`dots ${props.className}`}>
            {Array.from(new Array(props.amount)).map((_, index) => (
                <div key={index} className={`dot ${index === props.active && "active"}`} />
            ))}
        </div>
    );
};

export default Dots;
