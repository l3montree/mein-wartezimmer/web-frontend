import React, { FunctionComponent, ReactNode } from "react";

interface Props {
    children: ReactNode;
}

const TabPanel: FunctionComponent<Props> = (props: Props) => {
    return <div className={"tab-panel"}>{props.children}</div>;
};

export default TabPanel;
