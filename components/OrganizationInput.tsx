import React from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";

interface Props<T> {
    className?: string;
    onSelect: (_, org: T) => void;
    onChange: (e) => void;
    options: T[];
}

function OrganizationInput<T extends { title: string }>(props: Props<T>) {
    return (
        <Autocomplete
            options={props.options}
            freeSolo={true}
            className={props.className}
            onChange={props.onSelect}
            filterOptions={o => o}
            getOptionLabel={option => option.title ?? ""}
            renderInput={params => (
                <TextField
                    {...params}
                    label="Einrichtung, Unternehmen, Ort, ..."
                    variant="outlined"
                    onChange={props.onChange}
                    helperText={
                        "Suche nach der Einrichtung, dem Unternehmen, dem Ort oder Ähnlichem, wo du eine Wartenummer benötigst"
                    }
                    inputProps={{ ...params.inputProps, autoComplete: "new-password" }}
                />
            )}
        />
    );
}

export default OrganizationInput;
