import React, { FunctionComponent, useContext } from "react";
import { Ticket as SharedTicket } from "../shared";
import { ListItem, ListItemSecondaryAction, ListItemText } from "@material-ui/core";
import { AppContext } from "../context/AppContext";
import { useRouter } from "next/router";
import LocalOfferOutlinedIcon from "@material-ui/icons/LocalOfferOutlined";
import Ticket from "../api-layer/Ticket";

interface Props extends SharedTicket {
    onClick?: () => void;
}
const LocalTicketItem: FunctionComponent<Props> = (props: Props) => {
    const { dispatch } = useContext(AppContext);
    const router = useRouter();
    const handleClick = async () => {
        router.push("/");
        dispatch({
            type: "SET_TICKET",
            payload: {
                ...(await Ticket.get(props.id.toString())),
                currentTime: new Date(),
            },
        });
        props.onClick && props.onClick();
    };
    return (
        <div className={"px-3"}>
            <ListItem onClick={handleClick} className={"local-ticket-item rounded bg-light my-3"}>
                <ListItemText primary={props.id} />
                <ListItemSecondaryAction>
                    <LocalOfferOutlinedIcon />
                </ListItemSecondaryAction>
            </ListItem>
        </div>
    );
};

export default LocalTicketItem;
