import React, { FunctionComponent, useEffect, useState } from "react";
import { Organization } from "../shared";
import { Section } from "../shared/auth";
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    TextField,
} from "@material-ui/core";
import OrganizationInfo from "./OrganizationInfo";
import { useInput } from "../hooks/useInput";

interface Props {
    onDraw: ({ sectionId: number, title: string }) => void;
    open: boolean;
    onClose: () => void;
    org?: Organization & { __sections__: Section[] };
}
const DrawTicketDialog: FunctionComponent<Props> = (props: Props) => {
    const { open, onClose, org } = props;
    const [selected, select] = useState(org && org.__sections__ ? org.__sections__[0].id : 0);
    const title = useInput();

    useEffect(() => {
        if (props.org) {
            select(props.org.__sections__[0].id);
        }
    }, [props.org]);
    return (
        <Dialog open={open} onClose={onClose}>
            {org && (
                <>
                    <DialogTitle>{org.title}</DialogTitle>
                    <DialogContent>
                        <OrganizationInfo {...org} />
                        {org.__sections__.length > 1 && (
                            <>
                                <h5 className={"mb-4"}>Sektion auswählen</h5>
                                <FormControl className={"w-100"} variant="outlined">
                                    <InputLabel htmlFor="outlined-age-native-simple">Raum / Abteilung / ...</InputLabel>
                                    <Select
                                        className={"w-100"}
                                        value={selected}
                                        onChange={e => select(e.target.value as number)}
                                        label="Raum / Abteilung / ..."
                                        inputProps={{
                                            name: "age",
                                            id: "outlined-age-native-simple",
                                        }}>
                                        {org.__sections__.map(sec => (
                                            <MenuItem key={sec.id} value={sec.id}>
                                                {sec.title}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            </>
                        )}
                        <hr />
                        <h5 className={"mb-4"}>Persönliche Informationen (optional)</h5>
                        <TextField
                            className={"w-100"}
                            variant={"outlined"}
                            value={title.value}
                            onChange={title.onChange}
                            label={"Name"}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={onClose} disableElevation={true} color={"default"} variant={"contained"}>
                            Schließen
                        </Button>
                        <Button
                            onClick={() => props.onDraw({ sectionId: selected, title: title.value ?? "-" })}
                            disableElevation={true}
                            color={"primary"}
                            variant={"contained"}>
                            Nummer ziehen
                        </Button>
                    </DialogActions>
                </>
            )}
        </Dialog>
    );
};

export default DrawTicketDialog;
