import React from "react";
import Page from "../layout/Page";
import LoyaltyTwoToneIcon from "@material-ui/icons/LoyaltyTwoTone";
import { LinearProgress } from "@material-ui/core";

const Loading = () => {
    return (
        <Page description={"Lädt..."} title={"Lädt..."}>
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <div
                            className={
                                "loading-container bg-light mt-5 d-flex justify-content-center align-items-center"
                            }>
                            <div className={"d-flex flex-column align-items-center"}>
                                <div className="lds-heart text-danger">
                                    <div></div>
                                </div>
                                <div className={"mt-5"}>
                                    <p>
                                        <LoyaltyTwoToneIcon color={"error"} />
                                        <b className={"pl-2"}>mein-wartezimmer.de</b>
                                    </p>
                                    <LinearProgress color="secondary" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default Loading;
