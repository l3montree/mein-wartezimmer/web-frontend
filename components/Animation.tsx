import React, { FunctionComponent, useEffect, useRef } from "react";
import lottie from "lottie-web";

interface Props {
    animationData: any;
    className?: string;
    loop: boolean;
    autoplay: boolean;
}
const Animation: FunctionComponent<Props> = (props: Props) => {
    const container = useRef(null);
    useEffect(() => {
        lottie.loadAnimation({
            container: container.current,
            renderer: "svg",
            loop: props.loop,
            autoplay: props.autoplay,
            animationData: props.animationData,
        });
    }, []);
    return <div className={`animation ${props.className}`} ref={container} />;
};

export default Animation;
