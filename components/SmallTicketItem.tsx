import React, { FunctionComponent } from "react";
import { Checkbox, Chip, IconButton, Menu, MenuItem } from "@material-ui/core";
import { Ticket, TicketState } from "../shared";
import moment from "moment";
import DragHandleIcon from "@material-ui/icons/DragHandle";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import { Section } from "../shared/auth";

interface Props extends Ticket {
    section: Section;
}
const SmallTicketItem: FunctionComponent<Props> = (props: Props) => {
    return (
        <div className={"row ticket-item card flex-row align-items-center"}>
            <div className={"col-md-7"}>{props.title}</div>
            <div className={"col-md-5 text-right"}>
                <b>{props.id}</b>
            </div>
        </div>
    );
};

export default SmallTicketItem;
