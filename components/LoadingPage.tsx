import React from "react";
import LoyaltyTwoToneIcon from "@material-ui/icons/LoyaltyTwoTone";
import { LinearProgress } from "@material-ui/core";
import Head from "next/head";

const LoadingPage = () => {
    return (
        <>
            <Head>
                <title>Lädt... - mein-wartezimmer.de</title>
                <meta name="description" content="Lädt..." />
            </Head>
            <div className={"loading-container bg-light vh-100 d-flex justify-content-center align-items-center"}>
                <div className={"d-flex flex-column align-items-center"}>
                    <div className="lds-heart text-danger">
                        <div></div>
                    </div>
                    <div className={"mt-5"}>
                        <p>
                            <LoyaltyTwoToneIcon color={"error"} />
                            <b className={"pl-2"}>mein-wartezimmer.de</b>
                        </p>
                        <LinearProgress color="secondary" />
                    </div>
                </div>
            </div>
        </>
    );
};

export default LoadingPage;
