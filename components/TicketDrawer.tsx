import React, { FunctionComponent } from "react";
import { Drawer } from "@material-ui/core";
import TicketList from "./TicketList";

interface Props {
    open: boolean;
    onClose: () => void;
}

const TicketDrawer: FunctionComponent<Props> = (props: Props) => {
    return (
        <Drawer {...props} anchor={"right"}>
            <div className={"p-4"}>
                <h2>Wartenummern</h2>
                <hr />
                <TicketList />
            </div>
        </Drawer>
    );
};

export default TicketDrawer;
