import React, { FunctionComponent, ReactElement } from "react";

interface Props {
    children: ReactElement[];
    active: number;
}

const TabContainer: FunctionComponent<Props> = (props: Props) => {
    return (
        <div className={"tab-container position-relative"}>
            <div
                style={{
                    transform: `translateX(-${props.active * (1 / props.children.length) * 100}%)`,
                    width: `${props.children.length * 100}%`,
                }}>
                {props.children}
            </div>
        </div>
    );
};

export default TabContainer;
