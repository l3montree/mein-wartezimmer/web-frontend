import React, { FunctionComponent } from "react";
import { IconButton } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
interface Props {
    closeToast?: () => void;
}
const CloseButton: FunctionComponent<Props> = ({ closeToast }) => {
    return (
        <IconButton onClick={closeToast}>
            <CloseIcon style={{ color: "white" }} />
        </IconButton>
    );
};

export default CloseButton;
