import React, { FunctionComponent, useContext, useEffect, useState } from "react";
import { Button, MenuItem, Select, TextField } from "@material-ui/core";
import { AppContext } from "../context/AppContext";
import { useInput } from "../hooks/useInput";
import Ticket from "../api-layer/Ticket";
import Form from "./Form";

interface Props {
    sectionId: number;
    onSubmit: (ticket: Ticket, values: { title: string; sectionId: number }) => void;
}

const TicketForm: FunctionComponent<Props> = (props: Props) => {
    const { organization } = useContext(AppContext);
    const title = useInput({ initial: "" });

    const handleSubmit = async () => {
        const ticket = new Ticket(organization.id);
        title.onChange({ target: { value: "" } });
        props.onSubmit(ticket, { title: title.value, sectionId: props.sectionId });
    };

    return (
        <Form className={"card d-flex flex-row"} onSubmit={handleSubmit}>
            <TextField
                value={title.value}
                onChange={title.onChange}
                className={"w-100 mr-3"}
                variant={"outlined"}
                label={"Besucher"}
            />
            <Button type={"submit"} disableElevation={true} variant={"contained"} color={"primary"}>
                Erstellen
            </Button>
        </Form>
    );
};

export default TicketForm;
