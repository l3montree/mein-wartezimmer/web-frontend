import React, { FunctionComponent } from "react";
import { Organization, Ticket as SharedTicket } from "../shared";
import { Section } from "../shared/auth";
import { Button, LinearProgress } from "@material-ui/core";
import moment from "moment";
import washHands from "../animations/wash-hands.json";
import Animation from "./Animation";
import OrganizationInfo from "./OrganizationInfo";

interface Props extends SharedTicket {
    time: number;
    position: number;
    organization: Organization;
    section: Section;
    currentTime: Date;
    onClear: () => void;
}

const TicketInfoContainer: FunctionComponent<Props> = (props: Props) => {
    return (
        <div className={"container min-height-100 d-flex align-items-center"} id={"ticket-container"}>
            <div className={"w-100"}>
                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <div className={"text-center mb-5"}>
                            <h5>Wartenummer:</h5>
                            <h1>{props.id}</h1>
                            <p>
                                Bitte sei pünktlich in der Praxis: <b>{props.organization.title}</b>
                            </p>
                        </div>
                        <div className={"card text-right"}>
                            <LinearProgress
                                color={"primary"}
                                variant="determinate"
                                value={100 - (props.time < 0 ? 0 : props.time)}
                            />
                            <p className={"mt-3 mb-0"}>
                                <b>Noch {props.position} vor Dir</b>
                                <br />
                                Behandelt vorraussichtlich in:{" "}
                                <span className={props.time < 0 ? "text-danger font-weight-bold" : undefined}>
                                    {props.time} Minuten (
                                    {moment()
                                        .add(props.time, "minutes")
                                        .format("HH:mm")}
                                    )
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
                <div className={"row mt-5"}>
                    <div className={"col-md-6"}>
                        <h5>Infos zur Praxis</h5>
                        <OrganizationInfo {...props.organization} />
                    </div>
                    <div className={"col-md-4 d-flex justify-content-end"}>
                        <Animation
                            className={"wash-hands-animation"}
                            animationData={washHands}
                            loop={true}
                            autoplay={true}
                        />
                    </div>
                    <div className={"col-md-2"}>
                        <p>Denk dran regelmäßig deine Hände zu waschen!</p>
                    </div>
                </div>
                <div className={"row mt-5"}>
                    <div className={"col-md-12"}>
                        <Button
                            className={"w-100"}
                            disableElevation={true}
                            color={"secondary"}
                            variant={"contained"}
                            onClick={props.onClear}>
                            Wartenummer entfernen
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default TicketInfoContainer;
