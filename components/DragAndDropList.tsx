import React, { FunctionComponent, useContext, useState } from "react";
import FadeIn from "./FadeIn";
import TicketItem from "./TicketItem";
import { Ticket as SharedTicket, TicketState, TicketType } from "../shared";
import { Section } from "../shared/auth";
import Ticket from "../api-layer/Ticket";
import { useFetchInfinite } from "../hooks/useFetchInfinite";
import { toast } from "react-toastify";
import { AppContext } from "../context/AppContext";
import SmallTicketItem from "./SmallTicketItem";
import { Button } from "@material-ui/core";
import { DragDropContext, Draggable, Droppable, DropResult } from "react-beautiful-dnd";
import TicketForm from "./TicketForm";
import useWindowDimensions from "../hooks/useWindowDimensions";
import Utils from "../statics/Utils";
import moment from "moment";

interface Props {
    section: Section;
    state: TicketState;
    // determines if the current list is the list displayed on mobile
    open: boolean;
    // only list displayed.
    single: boolean;
}
const DragAndDropList: FunctionComponent<Props> = (props: Props) => {
    const { organization } = useContext(AppContext);
    const { width } = useWindowDimensions();
    const [lastAdded, setLastAdded] = useState<SharedTicket[]>([]);
    const [delays, setDelays] = useState([
        { uniqId: Utils.uniqId(), delay: TicketType.smallDelay },
        { uniqId: Utils.uniqId(), delay: TicketType.mediumDelay },
        { uniqId: Utils.uniqId(), delay: TicketType.bigDelay },
    ]);

    const handleSubmit = async (ticket: Ticket, values: { title: string; sectionId: number }) => {
        const res = await ticket.store({ ...values, title: values.title ?? "-" });
        list.dispatch({
            type: "UNCONVENTIONAL_replace",
            payload: { ...list.data, content: [...list.data.content, res] },
        });
        setLastAdded(prevState => {
            const arr = [res, ...prevState];
            arr.slice(0, 5);
            return arr;
        });
        toast.success("Eintrag wurde erfolgreich hinzugefügt");
    };

    const list = useFetchInfinite<Omit<SharedTicket, "id"> & { className?: string; id: string | number }>({
        operation: query => {
            const params = new URLSearchParams({
                ...query,
                state: props.state.toString(),
                sectionId: props.section.id.toString(),
            });
            return new Ticket(organization.id).filter(params);
        },
        deps: [props.state],
        interval: 10000,
    });

    const calculateTime = (arr: SharedTicket[]): SharedTicket[] => {
        if (arr.length === 0) {
            return arr;
        }
        return arr.map((item, index) => {
            let time = Math.ceil(
                moment
                    .duration(
                        moment(item.createdAt)
                            .utc()
                            .diff(moment().utc()),
                    )
                    .asMinutes(),
            );
            for (let i = 0; i < index; i++) {
                switch (arr[i].type) {
                    case TicketType.bigDelay:
                        time += TicketType.bigDelay;
                        break;
                    case TicketType.mediumDelay:
                        time += TicketType.mediumDelay;
                        break;
                    case TicketType.smallDelay:
                        time += TicketType.smallDelay;
                        break;
                    case TicketType.usual:
                        time += organization.averageTimeNeeded;
                        break;
                }
            }
            return { ...item, time };
        });
    };

    const handleDragEnd = (dropResult: DropResult) => {
        if (!dropResult.destination || dropResult.destination.droppableId.startsWith("delay")) {
            return;
        }
        if (dropResult.source.droppableId === dropResult.destination.droppableId) {
            const dest: number = +list.data.content[dropResult.destination.index].id;
            const content = Utils.reorder(list.data.content, dropResult.source.index, dropResult.destination.index);

            if (dropResult.destination.index > dropResult.source.index) {
                new Ticket(organization.id).moveAfter(+dropResult.draggableId, dest).catch(console.error);
            } else {
                new Ticket(organization.id).moveBefore(+dropResult.draggableId, dest).catch(console.error);
            }
            list.data.updateItems({ ...list.data, content });
        } else if (dropResult.destination.index < list.data.content.length) {
            // the user drops a delay inside the list
            // we have to CREATE the ticket.
            let type;
            switch (dropResult.draggableId) {
                case "delay-5":
                    type = TicketType.smallDelay;
                    break;
                case "delay-10":
                    type = TicketType.mediumDelay;
                    break;
                default:
                    type = TicketType.bigDelay;
            }
            const moveBefore = list.data.content[dropResult.destination.index].id;
            const data = {
                title: `+${type} min`,
                type,
                sectionId: props.section.id,
                position: 0,
                createdAt: new Date(),
                state: props.state,
            };
            const uniqId = Utils.uniqId();
            const content = [...list.data.content];
            content.splice(dropResult.destination.index, 0, { ...data, id: uniqId });
            list.data.updateItems({ ...list.data, content });
            setDelays(prevState => {
                return prevState.filter(t => t.delay !== type);
            });

            setTimeout(async () => {
                await new Ticket(organization.id).store(data).then(res => {
                    content[dropResult.destination.index] = res;
                    list.data.updateItems({ ...list.data, content });
                    new Ticket(organization.id).moveBefore(res.id, +moveBefore);
                    setDelays(prevState => [...prevState, { uniqId: Utils.uniqId(), delay: type }]);
                });
            }, 1000);
        }
    };

    const handleDelete = async (id: number, index: number) => {
        await new Ticket(organization.id).destroy({ id });
        removeItemFromList(index);
        toast.success("Erfolgreich gelöscht");
    };

    const handleRestore = async (id: number, index: number) => {
        await new Ticket(organization.id).patch({ state: TicketState.pending, id });
        removeItemFromList(index);
        toast.success("Erfolgreich wiederhergestellt");
    };

    const handleFinishChange = async (id: number, index: number, state: TicketState) => {
        await new Ticket(organization.id).patch({ state, id });
        removeItemFromList(index);
    };

    const removeItemFromList = index => {
        const item = list.data.content[index];
        list.data.update({ ...item, className: "slide-out-right" }, index);
        setTimeout(() => {
            const content = [...list.data.content];
            content.splice(index, 1);
            list.dispatch({ type: "UNCONVENTIONAL_replace", payload: { ...list.data, content } });
        }, 500);
    };

    return (
        <>
            <div
                id={props.section.id.toString()}
                className={`drag-and-drop-list pb-4 ${props.single && "col-md-12 single-list"}`}>
                <DragDropContext onDragEnd={handleDragEnd}>
                    <div className={"pb-4 ticket-form"}>
                        {props.state === TicketState.pending && (
                            <>
                                <div className={"row d-none d-sm-flex mp-2"}>
                                    <div className={"col-md-12"}>
                                        <h5>Neuer Eintrag ({props.section.title})</h5>
                                    </div>
                                </div>

                                <div className={"row"}>
                                    <div className={"col-md-12"}>
                                        <TicketForm sectionId={props.section.id} onSubmit={handleSubmit} />
                                    </div>
                                </div>
                            </>
                        )}
                    </div>
                    <div className={width <= 576 ? "overflow-auto" : undefined}>
                        <div className={"row"}>
                            <div className={"col-lg-9"}>
                                <h5>Warteliste {props.state !== TicketState.pending && `(${props.section.title})`}</h5>
                                <hr />
                                <div className={"d-flex mb-3"}>
                                    <div className={"col-md-12 d-flex ticket-item"}>
                                        <div className={"col-sm-1 text-truncate d-flex align-items-center"}></div>
                                        <div className={"col-sm-3 text-truncate"}>Besucher</div>
                                        <div className={"col-sm-2 text-truncate"}>Raum / Abteilung / ...</div>
                                        <div className={"col-sm-2 text-truncate"}>
                                            <b>Wartenummer</b>
                                        </div>
                                        <div className={"col-sm-3 text-truncate"}>Bestellt für</div>
                                        <div className={"col-sm-1 d-flex justify-content-end"}></div>
                                    </div>
                                </div>
                                <Droppable droppableId={props.section.id.toString()}>
                                    {(dropProvided, snapshot) => (
                                        <div ref={dropProvided.innerRef} {...dropProvided.droppableProps}>
                                            {calculateTime(list.data.content as SharedTicket[]).map(
                                                (ticket: SharedTicket, index) => (
                                                    <Draggable
                                                        key={ticket.id}
                                                        draggableId={ticket.id.toString()}
                                                        index={index}>
                                                        {(provided, snapshot) => (
                                                            <div
                                                                className={
                                                                    snapshot.isDragging
                                                                        ? "dragging draggable pb-2"
                                                                        : "draggable pb-2"
                                                                }
                                                                ref={provided.innerRef}
                                                                {...provided.dragHandleProps}
                                                                {...provided.draggableProps}>
                                                                <FadeIn
                                                                    disabled={ticket.type !== TicketType.usual}
                                                                    delay={(index % 30) * 100}>
                                                                    <div className={"col-md-12"}>
                                                                        <TicketItem
                                                                            index={index}
                                                                            onRestore={handleRestore}
                                                                            onFinishChange={handleFinishChange}
                                                                            onDelete={handleDelete}
                                                                            {...(ticket as SharedTicket & {
                                                                                section: Section;
                                                                                time: number;
                                                                            })}
                                                                        />
                                                                    </div>
                                                                </FadeIn>
                                                            </div>
                                                        )}
                                                    </Draggable>
                                                ),
                                            )}
                                            {dropProvided.placeholder}
                                        </div>
                                    )}
                                </Droppable>
                            </div>
                            <div className={"d-none d-lg-block col-md-3"}>
                                <div className={"mb-5"}>
                                    <h5>Zuletzt hinzugefügt</h5>
                                    <hr />
                                    {lastAdded.map((ticket, index) => (
                                        <FadeIn key={ticket.id} delay={(index % 30) * 100}>
                                            <div className={"col-md-12 mb-2"}>
                                                <SmallTicketItem {...(ticket as SharedTicket & { section: Section })} />
                                            </div>
                                        </FadeIn>
                                    ))}
                                </div>
                                <h5>Verspätungen</h5>
                                <hr />
                                <Droppable droppableId={"delay"}>
                                    {(dropProvided, snapshot) => (
                                        <div ref={dropProvided.innerRef} {...dropProvided.droppableProps}>
                                            {delays
                                                .sort((a, b) => a.delay - b.delay)
                                                .map(({ delay, uniqId }, index) => (
                                                    <Draggable
                                                        key={uniqId}
                                                        draggableId={`delay-${delay.toString()}`}
                                                        index={index}>
                                                        {(provided, snapshot) => (
                                                            <div
                                                                className={
                                                                    snapshot.isDragging
                                                                        ? "dragging-delay draggable delay"
                                                                        : "draggable delay"
                                                                }
                                                                ref={provided.innerRef}
                                                                {...provided.dragHandleProps}
                                                                {...provided.draggableProps}>
                                                                <FadeIn delay={(index % 30) * 100}>
                                                                    <div className={"col-md-12 card mb-2 text-center"}>
                                                                        <h5 className={"m-0"}>+{delay} min</h5>
                                                                    </div>
                                                                </FadeIn>
                                                            </div>
                                                        )}
                                                    </Draggable>
                                                ))}
                                            {dropProvided.placeholder}
                                        </div>
                                    )}
                                </Droppable>
                            </div>
                        </div>
                    </div>
                    <div className={"row  mt-4"}>
                        <div className={"col-lg-9 d-flex justify-content-center"}>
                            {list.data.currentPage >= list.data.totalPages ? (
                                <p>🥳 Das war alles</p>
                            ) : (
                                <Button
                                    onClick={list.loadMore}
                                    disableElevation={true}
                                    variant={"contained"}
                                    color={"secondary"}>
                                    Mehr Laden
                                </Button>
                            )}
                        </div>
                    </div>
                </DragDropContext>
            </div>
        </>
    );
};

export default DragAndDropList;
