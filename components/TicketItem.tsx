import React, { FunctionComponent } from "react";
import { Checkbox, Chip, IconButton, Menu, MenuItem } from "@material-ui/core";
import { Ticket, TicketState, TicketType } from "../shared";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import { Section } from "../shared/auth";
import moment from "moment";

const TicketContent: FunctionComponent<Props> = (props: Props) => {
    return (
        <>
            <div className={"col-3"}>{props.title}</div>
            <div className={"col-2"}>
                <Chip color={"default"} label={props.section.title} title={props.section.title} />
            </div>
            <div className={"col-2"}>
                <b>{props.id}</b>
            </div>
            <div className={`col-3`}>
                <span className={props.time < 0 ? "text-danger font-weight-bold" : undefined}>{props.time} min</span> (
                {moment()
                    .add(props.time, "minutes")
                    .format("HH:mm")}
                )
            </div>
        </>
    );
};

const DelayTicketContent: FunctionComponent<Props> = (props: Props) => (
    <>
        <div className={"col-10 text-center"}>
            <h5 className={"m-0"}>{props.title}</h5>
        </div>
    </>
);

interface Props extends Ticket {
    section: Section;
    index: number;
    time: number;
    className?: string;
    onFinishChange: (id: number, index: number, newState: TicketState) => void;
    onDelete: (id: number, index: number) => void;
    onRestore: (id: number, index: number) => void;
}
const TicketItem: FunctionComponent<Props> = (props: Props) => {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    const handleDelete = () => {
        props.onDelete(props.id, props.index);
    };
    const handleRestore = () => {
        props.onRestore(props.id, props.index);
    };

    const handleFinishChange = e => {
        props.onFinishChange(
            props.id,
            props.index,
            props.state === TicketState.finished ? TicketState.pending : TicketState.finished,
        );
    };
    return (
        <div
            className={`${props.className} row ticket-item card flex-row align-items-center ${
                props.type !== TicketType.usual ? "ticket-delay bg-danger text-light" : undefined
            }`}>
            <div className={"col-1 d-flex align-items-center"}>
                {props.state !== TicketState.deleted && (
                    <Checkbox defaultChecked={TicketState.finished === props.state} onChange={handleFinishChange} />
                )}
            </div>
            {props.type !== TicketType.usual ? <DelayTicketContent {...props} /> : <TicketContent {...props} />}
            <div className={"col-1 d-flex justify-content-end"}>
                <IconButton onClick={e => setAnchorEl(e.currentTarget)}>
                    <MoreHorizIcon />
                </IconButton>
                <Menu onClose={() => setAnchorEl(null)} anchorEl={anchorEl} open={Boolean(anchorEl)}>
                    {props.state === TicketState.deleted ? (
                        <>
                            <MenuItem onClick={handleRestore}>Wiederherstellen</MenuItem>
                            <MenuItem onClick={handleDelete}>Endgültig löschen</MenuItem>
                        </>
                    ) : (
                        <MenuItem onClick={handleDelete}>Löschen</MenuItem>
                    )}
                </Menu>
            </div>
        </div>
    );
};

export default TicketItem;
