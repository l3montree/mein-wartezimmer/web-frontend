import React, { FunctionComponent } from "react";
import { Organization } from "../shared";

const OrganizationInfo: FunctionComponent<Organization> = (props: Organization) => {
    return (
        <>
            <p>{props.title}</p>
            <p>
                {props.road} {props.house_number}
                <br />
                {props.postcode} {props.town}
            </p>
            <p>
                Tel.: <a href={`tel:${props.phoneNumber}`}>{props.phoneNumber}</a>
            </p>
        </>
    );
};

export default OrganizationInfo;
