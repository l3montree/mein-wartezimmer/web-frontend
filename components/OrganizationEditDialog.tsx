import React, { FunctionComponent, useContext, useState } from "react";
import LoyaltyTwoToneIcon from "@material-ui/icons/LoyaltyTwoTone";
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControlLabel,
    IconButton,
    InputAdornment,
    Switch,
    TextField,
} from "@material-ui/core";
import TabPanel from "../components/TabPanel";
import Dots from "../components/Dots";
import TabContainer from "../components/TabContainer";
import AddCircle from "@material-ui/icons/AddCircle";
import HighlightOffOutlinedIcon from "@material-ui/icons/HighlightOffOutlined";
import Utils from "../statics/Utils";
import { useInput } from "../hooks/useInput";
import { Validator } from "../statics/Validator";
import { AuthService } from "../statics/AuthService";
import { toast } from "react-toastify";
import { AppContext } from "../context/AppContext";
import { useRouter } from "next/router";
import Organization from "../api-layer/Organization";
import { OpenStreetMapResponse } from "../shared";
import AddressInput from "./AddressInput";

interface Props {
    open: boolean;
    onClose: () => void;
}

const OrganizationEditDialog: FunctionComponent<Props> = (props: Props) => {
    const { dispatch, organization } = useContext(AppContext);
    const [sections, setSections] = useState<{ value: string; key: string; real?: boolean }[]>(
        organization.sections.map(section => ({ value: section.title, key: section.id.toString(), real: true })),
    );
    const [publicAccessible, setPublicAccessible] = useState(organization.publicAccessible);
    const [activeSlide, setActiveSlide] = useState(0);
    const email = useInput({ initial: organization.email, validator: Validator.isEmail });
    const email1 = useInput({
        validator: (value: string) =>
            (!(value && value.length > 0) && email.value === organization.email) ||
            Validator.isSame(value, email.value),
    });
    const password = useInput<string>({
        validator: (value: string) => !(value && value.length > 0) || Validator.password(value),
    });
    const password1 = useInput({
        validator: (value: string) => Validator.isSame(value, password.value, true),
    });

    const [address, setAddress] = useState<OpenStreetMapResponse>({
        ...organization,
        display_name: organization.address,
        address: organization,
    });

    const title = useInput({ initial: organization.title, validator: Validator.notEmpty });
    const phoneNumber = useInput({ initial: organization.phoneNumber, validator: Validator.notEmpty });
    const averageTimeNeeded = useInput<number>({
        initial: organization.averageTimeNeeded,
        validator: Validator.notEmpty,
    });
    const leadTime = useInput<number>({ initial: organization.leadTime });

    const handleSubmit = async () => {
        if (!sections.length || (sections.length === 1 && sections[0].value.length === 0)) {
            return toast.warn("Bitte erstelle mindestens einen Raum / Abteilung / ... der Praxis");
        }
        if (!address) {
            return toast.warn("Bitte trage eine Adresse ein");
        }

        try {
            const syncResponse = await new Organization().patch({
                ...address.address,
                id: organization.id,
                title: title.value,
                phoneNumber: phoneNumber.value,
                averageTimeNeeded: averageTimeNeeded.value,
                password: password.value ? password.value : undefined,
                email: email.value,
                sections: sections,
                publicAccessible,
            });
            dispatch({ type: "SUCCESSFUL_SYNC", payload: syncResponse });
            toast.success("Speichern erfolgreich!");
            props.onClose();
        } catch (e) {
            toast.error("Ein Fehler ist aufgetreten. Bitte versuche es später erneut.");
        }
    };
    const handleButtonClick = () => {
        if (activeSlide < 2) {
            return setActiveSlide(prevState => prevState + 1);
        }
        handleSubmit();
    };
    const handleBackButtonClick = () => {
        if (activeSlide > 0) {
            setActiveSlide(prevState => prevState - 1);
        }
    };

    const handleAddButtonClick = () => {
        setSections(prevState => [...prevState, { value: "", key: Utils.uniqId() }]);
    };

    const handleSectionChange = (e, index) => {
        setSections(prevState => {
            const newState = [...prevState];
            newState[index].value = e.target.value;
            return newState;
        });
    };

    const handleDeleteSection = (key: string) => {
        const clone = [...sections];
        setSections(clone.filter(section => section.key !== key));
    };

    return (
        <Dialog open={props.open} onClose={props.onClose}>
            <DialogTitle>
                <LoyaltyTwoToneIcon color={"error"} /> {organization.title} (bearbeiten)
            </DialogTitle>
            <DialogContent>
                <form className={"d-flex login-form justify-content-center flex-column w-100"} autoComplete={"off"}>
                    <TabContainer active={activeSlide}>
                        <TabPanel>
                            <h4 className={"mb-4"}>Einstellungen</h4>
                            <FormControlLabel
                                control={
                                    <Switch
                                        className={"ml-2"}
                                        color={"primary"}
                                        checked={publicAccessible}
                                        onChange={() => setPublicAccessible(prevState => !prevState)}
                                        name="publicAccessible"
                                    />
                                }
                                label="Nutzer können Wartenummern generieren"
                            />
                            <h4 className={"mb-4"}>Login Informationen</h4>
                            <div className={"d-flex flex-column"}>
                                <TextField
                                    {...email}
                                    autoComplete={"new-password"}
                                    className={"mb-4"}
                                    variant={"outlined"}
                                    label={"Email"}
                                />
                                <TextField
                                    {...email1}
                                    autoComplete={"new-password"}
                                    className={"mb-4"}
                                    variant={"outlined"}
                                    label={"Email Wdh."}
                                />
                                <TextField
                                    type={"password"}
                                    {...password}
                                    className={"mb-4"}
                                    variant={"outlined"}
                                    autoComplete={"new-password"}
                                    helperText={
                                        "Das Passwort muss aus mindestens 6 Zeichen bestehen, Groß - und Kleinbuchstaben wie Zahlen enthalten"
                                    }
                                    label={"Passwort (leer lassen für keine Änderung)"}
                                />
                                <TextField
                                    type={"password"}
                                    {...password1}
                                    className={"mb-4"}
                                    variant={"outlined"}
                                    autoComplete={"new-password"}
                                    label={"Passwort Wdh."}
                                />
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <h4 className={"mb-4"}>Allgemeine Informationen</h4>
                            <div className={"d-flex flex-column"}>
                                <TextField {...title} className={"mb-4"} variant={"outlined"} label={"Name"} />
                                <AddressInput selected={address} onSelect={setAddress} className={"mb-4"} />
                                <TextField
                                    {...phoneNumber}
                                    type={"tel"}
                                    className={"mb-4"}
                                    variant={"outlined"}
                                    label={"Telefonnummer"}
                                />
                            </div>
                            <div className={"row"}>
                                <div className={"col-md-6"}>
                                    <TextField
                                        {...averageTimeNeeded}
                                        type={"number"}
                                        className={"mb-4 w-100"}
                                        variant={"outlined"}
                                        label={"Durschnittl. Wartezeit in Minuten"}
                                    />
                                </div>
                                <div className={"col-md-6"}>
                                    <TextField
                                        {...leadTime}
                                        type={"number"}
                                        className={"mb-4 w-100"}
                                        variant={"outlined"}
                                        label={"Vorlaufzeit in Minuten (optional)"}
                                    />
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <h4 className={"mb-4"}>Struktur</h4>

                            {sections.map(({ key, value }, index) => (
                                <div className={"w-100"} key={index}>
                                    <TextField
                                        variant={"outlined"}
                                        key={key}
                                        className={"mb-4 w-100"}
                                        label={"Raum / Abteilung / ..."}
                                        value={value}
                                        InputLabelProps={{ shrink: value.length > 0 }}
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position={"end"}>
                                                    <IconButton onClick={() => handleDeleteSection(key)}>
                                                        <HighlightOffOutlinedIcon fontSize={"large"} />
                                                    </IconButton>
                                                </InputAdornment>
                                            ),
                                        }}
                                        onChange={e => {
                                            e.persist();
                                            handleSectionChange(e, index);
                                        }}
                                    />
                                </div>
                            ))}
                            <div className={"d-flex justify-content-end"}>
                                <IconButton onClick={handleAddButtonClick} color={"secondary"}>
                                    <AddCircle fontSize={"large"} />
                                </IconButton>
                            </div>
                        </TabPanel>
                    </TabContainer>
                    <div className={"d-flex w-100 justify-content-center align-items-center"}>
                        <Dots className={"position-absolute"} amount={3} active={activeSlide} />
                    </div>
                </form>
            </DialogContent>
            <DialogActions>
                {activeSlide > 0 && (
                    <Button
                        onClick={handleBackButtonClick}
                        color={"secondary"}
                        variant={"contained"}
                        disableElevation={true}>
                        Zurück
                    </Button>
                )}
                <Button
                    disabled={
                        activeSlide === 2 &&
                        (email1.error ||
                            email.error ||
                            !address ||
                            title.error ||
                            password.error ||
                            sections.length < 1 ||
                            password1.error)
                    }
                    onClick={handleButtonClick}
                    color={"primary"}
                    variant={"contained"}
                    disableElevation={true}>
                    {activeSlide === 2 ? "Speichern" : "Weiter"}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default OrganizationEditDialog;
