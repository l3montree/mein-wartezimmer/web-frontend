import React, { useContext, useEffect, useRef, useState } from "react";
import { Button, Drawer, IconButton } from "@material-ui/core";
import LoyaltyTwoToneIcon from "@material-ui/icons/LoyaltyTwoTone";
import AvTimerRoundedIcon from "@material-ui/icons/AvTimerRounded";
import ContactSupportRoundedIcon from "@material-ui/icons/ContactSupportRounded";
import AddCircleOutlined from "@material-ui/icons/AddCircleOutlined";
import Link from "next/link";
import { useRouter } from "next/router";
import { AppContext } from "../context/AppContext";
import { AuthService } from "../statics/AuthService";
import TicketDrawer from "../components/TicketDrawer";
import MenuRoundedIcon from "@material-ui/icons/MenuRounded";
import TicketList from "../components/TicketList";

const Navigation = () => {
    const router = useRouter();
    const header = useRef<HTMLElement>(null);
    const lastScroll = useRef(0);
    const [top, setTop] = useState(0);
    const { organization, dispatch } = useContext(AppContext);
    const [open, setOpen] = useState(false);
    const [ticketDrawerOpen, setTicketDrawerOpen] = useState(false);
    const handleButtonClick = async () => {
        await router.push("/login");
        if (organization.id > 0) {
            AuthService.logout();
            dispatch({ type: "LOGOUT" });
        }
    };

    const scrollListener = e => {
        const headerHeight = header.current.getBoundingClientRect().height;
        // set the last scroll correctly.
        const val = lastScroll.current - window.scrollY;
        if (val <= 0 && top >= 0) {
            setTop(-headerHeight);
        } else if (val >= 0 && top <= 0) {
            setTop(0);
        }
        lastScroll.current = window.scrollY;
    };

    useEffect(() => {
        window.addEventListener("scroll", scrollListener as EventListener);
        return () => window.removeEventListener("scroll", scrollListener);
    });

    return (
        <header
            style={{
                position: "fixed",
                top,
                left: 0,
                transition: "all 0.25s",
                right: 0,
                zIndex: 10,
                backgroundColor: "white",
            }}
            ref={header}>
            <TicketDrawer open={ticketDrawerOpen} onClose={() => setTicketDrawerOpen(false)} />
            <div className={"container-fluid"}>
                <div className={"row navigation"}>
                    <div className={"col-md-12 d-flex py-1 py-md-3 justify-content-between"}>
                        <div className={"d-flex align-items-center logo"}>
                            <Link href={"/"}>
                                <a className={"mr-4 d-md-inline"}>
                                    <LoyaltyTwoToneIcon color={"error"} />
                                    <b className={"pl-2"}>mein-wartezimmer.de</b>
                                </a>
                            </Link>
                        </div>
                        <div className={"d-none d-lg-flex links"}>
                            <Link href={"/"}>
                                <a className={`mr-2 mr-md-5 nav-item ${router.pathname === "/" && "active"}`}>
                                    <AvTimerRoundedIcon />
                                    <b className={"pl-2"}>Ticker</b>
                                </a>
                            </Link>
                            <Link href={"/organizations"}>
                                <a
                                    className={`mr-2 mr-md-5 nav-item ${router.pathname.includes("organizations") &&
                                        "active"}`}>
                                    <AddCircleOutlined />
                                    <b className={"pl-2"}>Wartenummer ziehen</b>
                                </a>
                            </Link>
                            <Link href={"/help"}>
                                <a className={`mr-2 mr-md-5 nav-item ${router.pathname === "/help" && "active"}`}>
                                    <ContactSupportRoundedIcon />
                                    <b className={"pl-2"}>Hilfe</b>
                                </a>
                            </Link>
                            <Button
                                className={"mr-2"}
                                onClick={() => setTicketDrawerOpen(true)}
                                variant={"contained"}
                                disableElevation={true}
                                color={"default"}>
                                Wartenummern
                            </Button>
                            {organization.id > 0 && (
                                <Button
                                    onClick={() =>
                                        router.push("/protected/[organization]", `/protected/${organization.slug}`)
                                    }
                                    className={"mr-2"}
                                    variant={"contained"}
                                    disableElevation={true}
                                    color={"primary"}>
                                    Dashboard
                                </Button>
                            )}
                            <Button
                                onClick={handleButtonClick}
                                variant={"contained"}
                                disableElevation={true}
                                color={organization.id > 0 ? "secondary" : "primary"}>
                                {organization.id > 0 ? "Logout" : "Login"}
                            </Button>
                        </div>
                        <div className={"d-flex d-lg-none"}>
                            <IconButton onClick={() => setOpen(true)}>
                                <MenuRoundedIcon />
                            </IconButton>
                        </div>
                        <Drawer anchor={"right"} onClose={() => setOpen(false)} open={open}>
                            <div className={"d-flex flex-column mt-5 links navigation"}>
                                <Link href={"/"}>
                                    <a
                                        className={`p-3 mr-md-5 nav-item ${router.pathname === "/" &&
                                            "active bg-light"}`}>
                                        <AvTimerRoundedIcon />
                                        <b className={"pl-2"}>Ticker</b>
                                    </a>
                                </Link>
                                <Link href={"/organizations"}>
                                    <a
                                        className={`p-3 mr-md-5 nav-item ${router.pathname.includes("organizations") &&
                                            "active bg-light"}`}>
                                        <AddCircleOutlined />
                                        <b className={"pl-2"}>Wartenummer ziehen</b>
                                    </a>
                                </Link>
                                <Link href={"/help"}>
                                    <a
                                        className={`p-3 mr-md-5 nav-item ${router.pathname === "/help" &&
                                            "active bg-light"}`}>
                                        <ContactSupportRoundedIcon />
                                        <b className={"pl-2"}>Hilfe</b>
                                    </a>
                                </Link>
                                <h6 className={"px-3 mt-3"}>Wartenummern</h6>
                                <TicketList />
                                {organization.id > 0 && (
                                    <Button
                                        onClick={() =>
                                            router.push("/protected/[organization]", `/protected/${organization.slug}`)
                                        }
                                        variant={"contained"}
                                        className={"mx-3 mt-3"}
                                        disableElevation={true}
                                        color={"primary"}>
                                        Dashboard
                                    </Button>
                                )}
                                <Button
                                    onClick={handleButtonClick}
                                    variant={"contained"}
                                    className={"m-3"}
                                    disableElevation={true}
                                    color={organization.id > 0 ? "secondary" : "primary"}>
                                    {organization.id > 0 ? "Logout" : "Login"}
                                </Button>
                            </div>
                        </Drawer>
                    </div>
                </div>
            </div>
        </header>
    );
};

export default Navigation;
