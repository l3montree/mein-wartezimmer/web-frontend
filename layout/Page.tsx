import React, { FunctionComponent, ReactNode, useEffect } from "react";
import Head from "next/head";
import Navigation from "./Navigation";
import Footer from "./Footer";

interface Props {
    title: string;
    description: string;
    faviconPath?: string;
    children: ReactNode;
}
const Page: FunctionComponent<Props> = (props: Props) => {
    return (
        <>
            <Head>
                <title>{props.title} - Mein Wartezimmer</title>
                <meta name="description" content={props.description} />
                <link rel="icon" href="/favicon.ico" />
                <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
                <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
                <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
                <script src={"/js/a.js"}></script>
            </Head>
            <Navigation />
            <main className={"bg-light"}>{props.children}</main>
            <Footer />
        </>
    );
};

export default Page;
