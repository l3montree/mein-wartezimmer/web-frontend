import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";

const Footer = () => {
    const router = useRouter();
    return (
        <footer>
            <div className={"container-fluid"}>
                <div className={"row"}>
                    <div className={"col-md-12 py-3 d-flex flex-wrap justify-content-between"}>
                        <div className={"d-flex mt-2 mt-md-0 order-2 order-md-1 align-items-center"}>
                            <a rel="noreferrer" target={"_blank"} href={"https://l3montree.com"}>
                                <b className={"pr-2 powered-by"}>Picked from</b>
                                <img
                                    alt={"L3montree Logo"}
                                    className={"l3montree"}
                                    src={"/img/l3montree_logo_horizontal.svg"}
                                />
                            </a>
                        </div>
                        <div className={"order-md-2 order-1"}>
                            <Link href={"/privacy-policy"}>
                                <a className={`mr-3 mr-md-5 ${router.pathname === "/privacy-policy" && "active"}`}>
                                    <b>Datenschutz</b>
                                </a>
                            </Link>
                            <Link href={"/imprint"}>
                                <a className={router.pathname === "/imprint" ? "active" : undefined}>
                                    <b>Impressum</b>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;
