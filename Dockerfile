# Production DOCKERFILE
# Maintainer: Tim Bastin <tim.bastin>

FROM node:13-alpine
LABEL maintainer="bastin.tim@gmail.com"

RUN apk --no-cache add g++ make libpng-dev

WORKDIR /usr/app/

ENV NODE_ENV production
ENV PORT 3000
ENV API_HOST "https://api.mein-wartezimmer.de"
EXPOSE 3000

COPY . .

RUN npm i

RUN npm run build

CMD [ "npm", "start" ]
