import React from "react";
import Page from "../layout/Page";

const PrivacyPolicy = () => {
    return (
        <Page title={"Impressum"} description={""}>
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <h2 className={"pb-3"}>Datenschutzerklärung</h2>
                        <h3>Allgemeine Hinweise</h3>
                        <p>
                            Der Schutz Ihrer personenbezogenen Daten bei der Nutzung unserer Webseite ist uns ein
                            wichtiges Anliegen. Hier erfahren Sie, welche Daten erhoben und verwendet werden. Diese
                            Datenschutzerklärung klärt Sie über die Art, den Umfang und Zweck der Verarbeitung von
                            personenbezogenen Daten (nachfolgend kurz „Daten“) innerhalb unserer Anwendung und der mit
                            ihr verbundenen Dienste, Funktionen und Inhalte auf.
                        </p>
                        <p>
                            Personenbezogene Daten sind alle Daten, mit denen Sie persönlich identifiziert werden
                            können. Im Hinblick auf die verwendeten Begrifflichkeiten, wie z.B. „Verarbeitung“ oder
                            „Verantwortlicher“ verweisen wir auf die Definitionen im Art. 4 der
                            Datenschutzgrundverordnung (DSGVO).
                        </p>
                        <h3>Ausschluss</h3>
                        <p>Für externe Links zu fremden Inhalten können wir keine Haftung übernehmen</p>
                        <h3>Kontaktdaten des Verantwortlichen</h3>
                        <p>
                            Verantwortlicher im Sinne der EU-Datenschutz-Grundverordnung (DSGVO) und weiterer nationaler
                            datenschutzrechtlicher Bestimmungen ist:
                        </p>
                        <p>
                            Sebastian Kawelke
                            <br />
                            In der Grächt 27
                            <br />
                            53127 Bonn
                        </p>
                        <h3>Erhebung personenbezogener Daten beim Besuch unseres Onlineangebots</h3>
                        <p>Unsere Datenverarbeitung basiert auf dem Prinzip des legitimen Interesses</p>
                        <p>
                            Verarbeiten der Daten hilft uns herauszufinden, was auf unserer Seite funktioniert und was
                            nicht. Zum Beispiel finden wir damit heraus, ob die Inhalte gut ankommen oder wie wir die
                            Struktur der Webseite verbessern können. Unser Team profitiert davon und kann darauf
                            reagieren. Aufgrund der Datenverarbeitung profitieren Sie somit von einer Webseite, die
                            laufend besser wird.
                        </p>
                        <p>
                            Grundsätzlich werden alle, nicht länger für unsere Aufgabenerfüllung notwendigen, Daten in
                            regelmäßigen Abständen gelöscht.
                        </p>
                        <p>
                            Grundlage für die Datenverarbeitung ist Art. 6 Abs. 1 lit. f DSGVO, der die Verarbeitung von
                            Daten zur Erfüllung eines Vertrags oder vorvertraglicher Maßnahmen gestattet.
                        </p>
                        <ol type={"a"}>
                            <li>
                                <b>Indirekte Datensammlung (Server Logs)</b>
                                <br />
                                <p>
                                    Bei dem Besuch unserer Webseite erfassen unsere Server sogenannte ServerLogDateien.
                                    Diese Dateien werden nur für die Dauer ihrer Sitzung gespeichert. Es können
                                    allerdings Zugriffe länger gespeichert werden die nicht legitim sind, also keine
                                    normale Nutzung unseres Dienstes darstellen. Dies geschieht zur Gefahrenabwehr und
                                    zur Sicherstellung der IT-Sicherheit unserer Systeme.
                                </p>
                                <p>
                                    Konkret könne solche Daten sein:
                                    <ul>
                                        <li>verwendetes Betriebssystem</li>
                                        <li>verwendeter Browser</li>
                                        <li>Datum und Uhrzeit der Serveranfrage</li>
                                        <li>IPv4-Adresse und Subnetz, IPv6-Adresse und Prefix</li>
                                    </ul>
                                </p>
                                <p>
                                    dein-wartezimmer.de erhebt und verwendet diese personenbezogenen Daten grundsätzlich
                                    nur, soweit dies zur Bereitstellung und Verbesserung unseres Onlineangebots, unserer
                                    Funktionen sowie unserer Inhalte erforderlich ist. Rechtsgrundlage für die Erhebung
                                    der Daten und deren Speicherung in Logfiles ist Art. 6 Abs. 1 S. 1 lit. f DSGVO.
                                </p>
                                <p>
                                    Die Erfassung der, oben genannten, Daten zur Bereitstellung der Website und deren
                                    Speicherung in Logfiles (temporär pro Sitzung) ist für den Betrieb des Angebots
                                    zwingend erforderlich, sodass seitens der Nutzenden keine Widerspruchsmöglichkeit
                                    besteht.
                                </p>
                            </li>
                            <li>
                                <b>
                                    Erhebung personenbezogener Daten beim Besuch unseres Onlineangebots (Nutzung als
                                    Gast ohne Account (Regelfall))
                                </b>
                                <br />
                                <p>
                                    Diese Webseite verwendet Matomo, eine Open Source, selbstgehostete Software um
                                    anonyme Nutzungsdaten für diese Webseite zu sammeln. Die Daten zum Verhalten unserer
                                    Besucher werden gesammelt um eventuelle Probleme wie nicht gefundene Seiten,
                                    Suchmaschinenprobleme oder unbeliebte Seiten herauszufinden. Sobald die Daten
                                    (Anzahl der Besucher die Fehlerseiten oder nur eine Seite sehen, usw.) verarbeitet
                                    werden, erzeugt Matomo Berichte für uns, damit wir darauf reagieren können.
                                    (Layoutveränderungen, neue Inhalte, usw.)
                                </p>
                                <p>
                                    Matomo verarbeitet die folgenden Daten:
                                    <ul>
                                        <li>Cookies</li>
                                        <li>
                                            Anonymisierte IP-Adressen indem die letzten 2 bytes entfernt werden (also
                                            198.51.0.0 anstatt 198.51.100.54)
                                        </li>
                                        <li>
                                            Pseudoanonymisierter Standort (basierend auf der anonymisierten IPAdresse
                                        </li>
                                        <li>Datum und Uhrzeit</li>
                                        <li>Titel der aufgerufenen Seite</li>
                                        <li>URL der aufgerufenen Seite</li>
                                        <li>URL der vorhergehenden Seite (sofern diese das erlaubt)</li>
                                        <li>Bildschirmauflösung</li>
                                        <li>Lokale Zeit</li>
                                        <li>Dateien die angeklickt und heruntergeladen wurden</li>
                                        <li>Externe Links</li>
                                        <li>Dauer des Seitenaufbaus</li>
                                        <li>Land, Region, Stadt (mit niedriger Genauigkeit aufgrund von IP-Adresse)</li>
                                        <li>Hauptsprache des Browsers</li>
                                        <li>User Agent des Browsers</li>
                                        <li>Interaktionen mit Formularen (aber nicht deren Inhalt)</li>
                                    </ul>
                                </p>
                                <p>
                                    Wir möchten an dieser Stelle darauf hinweisen, dass wenn du als Nutzer von einer
                                    Einrichtung eine Wartenummer persönlich bekommst, diese die Möglichkeit hat, deinen
                                    Namen zu der jeweiligen Wartenummer zu speichern. Wenn du dem Wiedersprechen
                                    möchtest, merke das gegenüber deiner Einrichtung an – die Nutzung für dich und deine
                                    Einrichtung wird dadurch nicht beeinträchtigt.
                                </p>
                            </li>
                            <li>
                                <b>
                                    Erhebung personenbezogener Daten bei der Nutzung unseres Onlineangebots als
                                    Einrichtung (Nutzung mit Account)
                                </b>
                                <br />
                                <p>
                                    Bei der Nutzung unseres Angebotes mit einem Account (bei der Registrierung eines
                                    Accounts), werden Daten folgende Daten von uns erfasst:
                                    <ul>
                                        <li>Name der Einrichtung</li>
                                        <li>E-Mail Adresse</li>
                                        <li>In für uns nicht lesbarer Form, ihr Passwort</li>
                                        <li>Die Adresse der Einrichtung (zusätzlich in Koordinatenform)</li>
                                        <li>Deine angegebene Telefonnummer</li>
                                        <li>Eingetragene durchschnittliche Wartezeit</li>
                                        <li>Eingetragene Vorlaufzeit</li>
                                        <li>Angegebene Räume/ Struktureinheiten</li>
                                        <li>Dir zugeordnete Wartenummern</li>
                                    </ul>
                                </p>
                                <p>
                                    Wir verwenden diese Daten ausschließlich zur Bereitstellung unseres Angebotes.
                                    Darüber hinaus werden diese Daten nicht verwendet. Neben den oben angegebenen Daten
                                    erfassen wir u.U. auch Daten wie unter Punkt a) Indirekte Datensammlung (Server
                                    Logs) und unter Punkt b) Erhebung personenbezogener Daten beim Besuch unseres
                                    Onlineangebots (Nutzung als Gast ohne Account (Regelfall)) angegeben.
                                </p>
                                <p>
                                    Grundlage für die Datenverarbeitung ist Art. 6 Abs. 1 lit. f DSGVO, der die
                                    Verarbeitung von Daten zur Erfüllung eines Vertrags oder vorvertraglicher Maßnahmen
                                    gestattet.
                                </p>
                            </li>
                            <li>
                                <b>Kontakt per E-Mail</b>
                                <br />
                                <p>
                                    Bei dem Kontakt z.B. per E-Mail zu uns, erfassen wir ihre Kontaktdaten, bis der
                                    Grund des Kontaktes seine Relevanz verliert und z.B. eine Rückantwort von unserer
                                    Seite aus nicht mehr notwendig ist. Unberührt ist hiervon natürlich Ihr Recht auf
                                    Einsicht sowie Löschung (s. „Ihre Rechte“). Für die Kontaktaufnahme per E-Mail ist
                                    anzumerken, dass die Vertraulichkeit von E-Mails oder anderen elektronischen
                                    Kommunikationsformen im Internet grundsätzlich nicht gewährleistet ist. Für
                                    vertrauliche Informationen empfehlen wir die Kontaktaufnahme in verschlüsselter Form
                                    (info@dein-wartezimmer.de) oder über den Postweg.
                                </p>
                            </li>
                        </ol>
                        <h3>Einsatz von Cookies</h3>
                        <p>
                            Wir verwenden Cookies ausschließlich zur Bereitstellung unserer Webseite in deiner
                            präferierten Sprache. Von dem Setzen von anderen Cookies bzw zu anderen Zwecken sehen wir
                            ab.
                        </p>
                        <p>
                            Du kannst die Speicherung der Cookies durch eine entsprechende Einstellung deiner
                            Browser-Software verhindern; wir weisen dich jedoch darauf hin, dass du in diesem Fall
                            gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich nutzen kannst. Du
                            kannst darüber hinaus die Erfassung der durch den Cookie erzeugten und auf deine Nutzung der
                            Website bezogenen Daten verhindern durch die Installation von speziellen Browser Plugins.
                        </p>
                        <h3>Weitergabe personenbezogener Daten an Dritte</h3>
                        <p>
                            Grundsätzlich geben wir keine Daten an Dritte weiter. Eventuell verlinken wir allerdings auf
                            Inhalte von Drittanbietern. Dabei handelt es sich zum Beispiel um Links zu Sozialen
                            Netzwerke wie Instagram und Twitter, die wichtig für die Verbreitung unserer Inhalte sind.
                            mein-wartezimmer.de schützt die Privatsphäre, indem keine direkte Einbindung der Inhalte von
                            Drittanbietern erfolgt. Nutzer, die unsere Seite verlassen und über bereitgestellte Links
                            Seiten Dritter besuchen sollten bedenken, dass auf diesen Seiten Dritter die jeweiligen
                            Datenschutzregelungen des Dritten gelten, auf die wir keinen Einfluss nehmen können. Wir
                            übernehmen keine Verantwortung für den Schutz ihrer Daten bei dem Besuch externer Webseiten,
                            Diensten oder Angeboten. Rechtsgrundlage für die Verarbeitung der Daten nach erfolgter
                            Einwilligung des Nutzers ist Art. 6 Abs. 1 S. 1 lit. a DSGVO.
                        </p>
                        <p>
                            Facebook:{" "}
                            <a rel="noreferrer" target="_blank" href="https://www.facebook.com/policy">
                                {" "}
                                https://www.facebook.com/policy
                            </a>
                            <br />
                            Youtube:{" "}
                            <a rel="noreferrer" target="_blank" href="https://policies.google.com/privacy?hl=de">
                                https://policies.google.com/privacy?hl=de
                            </a>
                            <br />
                            Twitter:{" "}
                            <a rel="noreferrer" target="_blank" href="https://twitter.com/de/privacy">
                                https://twitter.com/de/privacy
                            </a>
                            <br />
                            Instagram:{" "}
                            <a rel="noreferrer" target="_blank" href="https://www.instagram.com/legal/privacy/">
                                https://www.instagram.com/legal/privacy/
                            </a>
                            <br />
                            GitLab:{" "}
                            <a rel="noreferrer" target="_blank" href="https://about.gitlab.com/privacy/">
                                https://about.gitlab.com/privacy/
                            </a>
                        </p>
                        <h3>Wie wir Ihre Daten schützen</h3>
                        <p>
                            Diese Webseite nutzt aus Sicherheitsgründen und zum Schutz der Übertragung vertraulicher
                            Inhalte, wie zum Beispiel Anfragen, die Sie an unseren Server senden, eine SSL-bzw. TLS
                            Verschlüsselung. Dieses Verfahren ist für sämtliche Kommunikation zwischen deinem Browser
                            und unseren Servern umgesetzt.
                        </p>
                        <p>
                            Wenn die SSL- bzw. TLS-Verschlüsselung aktiviert ist, können die Daten, die Sie an uns
                            übermitteln, nicht von Dritten mitgelesen werden.
                        </p>
                        <p>
                            Neben der Verschlüsselung während der Übertragung, schützen wir Ihre Daten, wie z.B. die
                            i.d.R. temporär gespeicherten Logdateien, auch auf unseren Servern.
                        </p>
                        <p>
                            Weitergehend sind auch unsere Mitarbeiter verpflichtet die Vertraulichkeit Ihrer Daten zu
                            wahren (z.B. bei der Bearbeitung einer Kontaktaufnahme durch Sie).
                        </p>
                        <p>
                            Auch wenn wir alle verfügbaren Sicherheitsmechanismen zur Anwendung bringen, weisen wir
                            darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per EMail)
                            Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch
                            Dritte ist nicht möglich.
                        </p>
                        <h3>Ihre Rechte</h3>
                        <p>
                            Widerruf Ihrer Einwilligung zur Datenverarbeitung Mit Nutzung unseres Angebotes stimmen sie
                            der Verarbeitung, der zur Bereitstellung des Angebots erhobenen Daten zu. Hier besteht für
                            Sie keine direkte Widerspruchsmöglichkeit. Sie könne aber natürlich eine Löschung sämtlicher
                            Daten einfordern.
                        </p>
                        <p>
                            Viele Datenverarbeitungsvorgänge sind nur mit Ihrer ausdrücklichen Einwilligung möglich. Sie
                            können eine bereits erteilte Einwilligung jederzeit widerrufen. Dazu reicht eine formlose
                            Mitteilung per E-Mail an uns. Die Rechtmäßigkeit der bis zum Widerruf erfolgten
                            Datenverarbeitung bleibt vom Widerruf unberührt.
                        </p>
                        <h3>Beschwerderecht bei der zuständigen Aufsichtsbehörde</h3>
                        <p>
                            Im Falle datenschutzrechtlicher Verstöße steht dem Betroffenen ein Beschwerderecht bei der
                            zuständigen Aufsichtsbehörde zu. Zuständige Aufsichtsbehörde in datenschutzrechtlichen
                            Fragen ist der Landesdatenschutzbeauftragte des Bundeslandes, in dem unser Unternehmen
                            seinen Sitz hat. Eine Liste der Datenschutzbeauftragten sowie deren Kontaktdaten können
                            folgendem Link entnommen werden:
                            https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html
                        </p>
                        <h3>Recht auf Datenübertragbarkeit</h3>
                        <p>
                            Sie haben das Recht, Daten, die wir auf Grundlage Ihrer Einwilligung oder in Erfüllung eines
                            Vertrags verarbeiten, an sich oder an einen Dritten in einem gängigen, maschinenlesbaren
                            Format aushändigen zu lassen. Sofern Sie die direkte Übertragung der Daten an einen anderen
                            Verantwortlichen verlangen, erfolgt dies nur, soweit es technisch machbar ist.
                        </p>
                        <h3>Auskunft, Sperrung, Löschung</h3>
                        <p>
                            Sie haben im Rahmen der geltenden gesetzlichen Bestimmungen jederzeit das Recht auf
                            unentgeltliche Auskunft über Ihre gespeicherten personenbezogenen Daten, deren Herkunft und
                            Empfänger und den Zweck der Datenverarbeitung und ggf. ein Recht auf Berichtigung, Sperrung
                            oder Löschung dieser Daten. Hierzu sowie zu weiteren Fragen zum Thema personenbezogene Daten
                            können Sie sich jederzeit unter der im Impressum angegebenen Adresse an uns wenden.
                        </p>
                        <h3>Gesetzlich vorgeschriebener Datenschutzbeauftragter</h3>
                        <p>
                            Sebastian Kawelke
                            <br />
                            In der Grächt 27,
                            <br />
                            53127 Bonn
                            <br />
                            Deutschland
                            <br />
                            E-Mail: <a href={"mailto:info@mein-wartezimmer.de"}>info@mein-wartezimmer.de</a>
                            <br />
                            Sollten Sie Fragen zum Datenschutz haben nutzen Sie bitte die im Impressum angegebenen
                            Kontaktdaten.
                        </p>
                        <h3>Weitere Angaben</h3>
                        <h4>Kriterien für die Festlegung von Speicherdauern</h4>
                        <p>
                            Ihre personenbezogenen Daten werden beim uns solange gespeichert, bis die Vertragsbeziehung
                            endgültig beendet ist, sich keine weiteren gegenseitigen Ansprüche mehr daraus ergeben
                            können und auch die gesetzlichen oder internen Aufbewahrungsfristen abgelaufen sind.
                        </p>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default PrivacyPolicy;
