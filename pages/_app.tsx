import React, { useEffect, useReducer, useState } from "react";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import "../styles/index.scss";
import { blue } from "@material-ui/core/colors";
import { toast, ToastContainer } from "react-toastify";
import { AppContext, AppReducer, defaultAppContext } from "../context/AppContext";
import CloseButton from "../components/CloseButton";
import { Logger } from "../statics/Logger";
import Utils from "../statics/Utils";
import Application from "../statics/Application";
import { useRouter } from "next/router";
import LoadingPage from "../components/LoadingPage";
import Storage from "../statics/Storage";

const App = ({ Component, pageProps }) => {
    const [state, dispatch] = useReducer(AppReducer, defaultAppContext);
    const router = useRouter();
    const [loading, setLoading] = useState(true);

    const theme = createMuiTheme({
        palette: {
            primary: blue,
            secondary: {
                main: "#464965",
                contrastText: "#ffffff",
            },
            text: {
                primary: "#464965",
            },
            // Used by `getContrastText()` to maximize the contrast between
            // the background and the text.
            contrastThreshold: 3,
            // Used by the functions below to shift a color's luminance by approximately
            // two indexes within its tonal palette.
            // E.g., shift from Red 500 to Red 300 or Red 700.
            tonalOffset: 0.2,
            error: {
                main: "#EF4F7F",
            },
        },
    });
    useEffect(() => {
        const tentacle = async () => {
            dispatch({ type: "SET_TICKETS", payload: Storage.getLocalTickets() });
            const successful = await Utils.safeInvoke(Application.boot);
            if (successful) {
                // user is logged in
                // we navigate him to the auth stack
                dispatch({ type: "SUCCESSFUL_SYNC", payload: successful });
                // await router.replace("/[organization]", `/${successful.slug}`);
                setTimeout(() => setLoading(false), 1000);
            } else if (router.pathname === "/protected/[organization]") {
                router.replace("/");
                setTimeout(() => setLoading(false), 1000);
            } else {
                setTimeout(() => setLoading(false), 1000);
            }
        };

        tentacle().catch(Logger.error);
    }, []);
    return (
        <ThemeProvider theme={theme}>
            <AppContext.Provider value={{ ...state, dispatch: dispatch }}>
                {loading ? <LoadingPage /> : <Component {...pageProps} />}
                <ToastContainer
                    closeButton={<CloseButton />}
                    hideProgressBar={true}
                    position={toast.POSITION.TOP_RIGHT}
                />
            </AppContext.Provider>
        </ThemeProvider>
    );
};

export default App;
