import React from "react";
import Page from "../layout/Page";
import Animation from "../components/Animation";
import Animation404 from "../animations/404.json";
import Link from "next/link";
import { useRouter } from "next/router";

const Page404 = () => {
    const router = useRouter();
    return (
        <Page
            description={"Wir konnten die Seite, nach der du suchst, leider nicht finden."}
            title={"404 - Nicht gefunden"}>
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-md-12 d-flex justify-content-center"}>
                        <Animation
                            className={"w-50 not-found w-50"}
                            animationData={Animation404}
                            loop={true}
                            autoplay={true}
                        />
                    </div>
                </div>
                <div className={"row"}>
                    <div className={"col-md-12 mt-3"}>
                        <h2 className={"text-center font-bold"}>
                            Wir konnten deine gesuchte Seite leider nicht finden...
                        </h2>
                        <p>Probiere es einmal hier:</p>
                        <ul>
                            <li>
                                <Link href={"/"}>
                                    <a className={`mr-md-5 nav-item ${router.pathname === "/" && "active bg-light"}`}>
                                        <b className={"pl-2"}>Ticker</b>
                                    </a>
                                </Link>
                            </li>
                            <li>
                                <Link href={"/organizations"}>
                                    <a
                                        className={`mr-md-5 nav-item ${router.pathname.includes("organizations") &&
                                            "active bg-light"}`}>
                                        <b className={"pl-2"}>Wartenummer ziehen</b>
                                    </a>
                                </Link>
                            </li>
                            <li>
                                <Link href={"/help"}>
                                    <a
                                        className={`mr-md-5 nav-item ${router.pathname === "/help" &&
                                            "active bg-light"}`}>
                                        <b className={"pl-2"}>Hilfe</b>
                                    </a>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default Page404;
