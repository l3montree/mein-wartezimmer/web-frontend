import React, { useContext } from "react";
import Page from "../layout/Page";
import { Button, TextField } from "@material-ui/core";
import LoyaltyTwoToneIcon from "@material-ui/icons/LoyaltyTwoTone";
import { useRouter } from "next/router";
import { useInput } from "../hooks/useInput";
import { AuthService } from "../statics/AuthService";
import { AppContext } from "../context/AppContext";
import { toast } from "react-toastify";
import Form from "../components/Form";

const Login = () => {
    const { dispatch } = useContext(AppContext);
    const router = useRouter();
    const email = useInput<string>();
    const password = useInput<string>();

    const handleLogin = async () => {
        try {
            const syncResponse = await AuthService.login(email.value, password.value);
            dispatch({ type: "SUCCESSFUL_SYNC", payload: syncResponse });
            router.push("/protected/[organization]", `/protected/${syncResponse.slug}`);
        } catch (e) {
            toast.error("E-Mail Adresse oder Passwort nicht korrekt");
        }
    };
    return (
        <Page
            description={
                "Mein Wartezimmer ermöglicht eine schnelle und strukturierte Organisation für das digitale Wartezimmer. Dieses intuitive Ticketsystem schafft Transparenz, Zufriedenheit bei den Besuchern und erleichtert die Arbeit in den Einrichtungen."
            }
            title={"Login"}>
            <div className={"container vh-100 d-flex align-items-center flex-column justify-content-center"}>
                <Form
                    onSubmit={handleLogin}
                    className={"d-flex card justify-content-center flex-column w-100 index-form"}
                    autoComplete={"off"}>
                    <h3 className={"text-center my-5"}>
                        <LoyaltyTwoToneIcon color={"error"} fontSize={"large"} /> Mein Wartezimmer
                    </h3>
                    <h4 className={"mb-4"}>Login</h4>
                    <TextField {...email} className={"mb-4"} variant={"outlined"} label={"Email"} />
                    <TextField
                        {...password}
                        type={"password"}
                        className={"mb-4"}
                        variant={"outlined"}
                        label={"Passwort"}
                    />
                    <Button type={"submit"} variant={"contained"} disableElevation={true} color={"primary"}>
                        Login
                    </Button>
                </Form>
                <div className={"my-4 w-100 d-flex align-items-center justify-content-center"}>
                    <div className={"border-div mr-5"} />
                    <b>Oder</b>
                    <div className={"border-div ml-5"} />
                </div>
                <Button
                    onClick={() => router.push("/register")}
                    className={"w-100"}
                    color={"secondary"}
                    variant={"contained"}
                    disableElevation={true}>
                    Einrichtung Registrieren
                </Button>
            </div>
        </Page>
    );
};

export default Login;
