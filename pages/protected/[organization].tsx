import React, { useContext, useState } from "react";
import { AppContext } from "../../context/AppContext";
import { TicketState } from "../../shared";
import SettingsOutlinedIcon from "@material-ui/icons/SettingsOutlined";
import { Button, Chip, IconButton } from "@material-ui/core";
import DragAndDropList from "../../components/DragAndDropList";
import Page from "../../layout/Page";
import OrganizationEditDialog from "../../components/OrganizationEditDialog";
import useWindowDimensions from "../../hooks/useWindowDimensions";
import Link from "next/link";
import { useQueryParam } from "../../hooks/useQueryParam";

const Organization = () => {
    const { organization } = useContext(AppContext);
    const { width } = useWindowDimensions();
    const [state, setState] = useQueryParam("state");
    const [selectedSection, selectSection] = useState(organization.sections[0].id);
    const [dialogOpen, setDialogOpen] = useState(false);
    const handleChipClick = (sectionId: number) => {
        const container = document.getElementById("list-container");
        const list = document.getElementById(sectionId.toString());
        selectSection(sectionId);
        if (list && container) {
            container.scrollTo({ left: list.offsetLeft, behavior: "smooth" });
        }
    };

    return (
        <Page description={`${organization.title} Dashboard`} title={organization.title}>
            <OrganizationEditDialog open={dialogOpen} onClose={() => setDialogOpen(false)} />
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-md-12 d-flex justify-content-between"}>
                        <h2 className={"text-truncate"}>{organization.title}</h2>
                        <IconButton onClick={() => setDialogOpen(true)} color={"default"}>
                            <SettingsOutlinedIcon />
                        </IconButton>
                    </div>
                </div>
                {organization.publicAccessible && (
                    <div className={"row"}>
                        <div className={"col-md-12"}>
                            Öffentlicher Einrichtungslink:{" "}
                            <Link href={"/organizations/[slug]"} as={`/organizations/${organization.slug}`}>
                                <a>
                                    {window.location.host}/organizations/{organization.slug}
                                </a>
                            </Link>
                        </div>
                    </div>
                )}
                <div className={"row mb-2"}>
                    <div className={"col-md-12 d-flex mt-4"}>
                        <Button
                            onClick={() => {
                                setState(TicketState.pending);
                            }}
                            className={"mr-2 text-truncate"}
                            color={+state === TicketState.pending ? "secondary" : "default"}
                            disableElevation={true}
                            variant={"contained"}>
                            Wartend
                        </Button>
                        <Button
                            onClick={() => {
                                setState(TicketState.finished);
                            }}
                            disableElevation={true}
                            className={"mr-2 text-truncate"}
                            color={+state === TicketState.finished ? "secondary" : "default"}
                            variant={"contained"}>
                            Abgeschlossen
                        </Button>
                        <Button
                            onClick={() => {
                                setState(TicketState.deleted);
                            }}
                            color={+state === TicketState.deleted ? "secondary" : "default"}
                            disableElevation={true}
                            variant={"contained"}>
                            Gelöscht
                        </Button>
                    </div>
                </div>
                <div className={"row mb-4"}>
                    <div className={"col-md-12"}>
                        {organization.sections.map(section => (
                            <Chip
                                key={section.id}
                                onClick={() => handleChipClick(section.id)}
                                className={"m-2"}
                                color={width <= 576 && selectedSection === section.id ? "secondary" : "default"}
                                label={section.title}
                            />
                        ))}
                    </div>
                </div>
            </div>
            <div
                className={`drag-and-drop-list-container ${
                    (width > 992 && organization.sections.length > 1) || width < 768 ? " container-fluid" : "container"
                }`}>
                <div
                    id={"list-container"}
                    className={`d-lg-flex mt-5 d-flex-row flex-nowrap ${width >= 992 && "overflow-auto"}`}>
                    {organization.sections
                        .filter(section => width >= 992 || section.id === selectedSection)
                        .map(section => (
                            <DragAndDropList
                                single={organization.sections.length === 1}
                                state={+state}
                                open={section.id === selectedSection}
                                key={section.id}
                                section={section}
                            />
                        ))}
                </div>
            </div>
        </Page>
    );
};

export default Organization;
