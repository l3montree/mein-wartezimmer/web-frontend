import React, { useState } from "react";
import Page from "../layout/Page";
import { Button, ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary } from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Link from "next/link";

const Help = () => {
    const [active, setActive] = useState("client");
    return (
        <Page
            title={"Hilfe"}
            description={"Hier erhältst du Antworten auf die häufigsten Fragen und Anleitungen zur Bedienung."}>
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <h2 className={"pb-3"}>Fragen & Antworten</h2>
                    </div>
                    <div className={"col-md-12 mb-3"}>
                        <Button
                            disableElevation={true}
                            variant={"contained"}
                            className={"mr-2"}
                            color={active === "client" ? "secondary" : "default"}
                            onClick={() => setActive("client")}>
                            Nutzer
                        </Button>
                        <Button
                            disableElevation={true}
                            variant={"contained"}
                            color={active === "organization" ? "secondary" : "default"}
                            onClick={() => setActive("organization")}>
                            Einrichtung
                        </Button>
                    </div>
                    {active === "client" ? (
                        <div className={"col-md-12 mb-5"}>
                            <h4>Nutzer</h4>
                            <ExpansionPanel>
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                    Meine Wartezeit hat sich verlängert. Woran liegt das?
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    Es kann vorkommen, dass es in deiner Ziel Einrichtung zu Verzögerungen kommt. Das
                                    ist z.B. der Fall, wenn in einer Praxis ein Notfallpatient dazwischenkommt. Wir
                                    bitten Dich um einen weiteren Moment Geduld.
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                            <ExpansionPanel>
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                    Kann ich hier auch meine zukünftigen Besuche planen?
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    Nein, dieses Tool bietet dir eine optimierte Form zur Organisation von flexiblen
                                    Wartevorgängen.
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                            <ExpansionPanel>
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                    Kann ich über diese Anwendung direkt Kontakt zur Einrichtung aufnehmen?
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    Du kannst die Einrichtung innerhalb der Öffnungszeiten über die angegebenen
                                    Kontaktdaten erreichen.
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                            <ExpansionPanel>
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                    Kann ich über diese Plattform auch meinen Termin vereinbaren oder verschieben?
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    Nein, diese Anwendung dient alleinig dazu, die Wartezeit transparenter zu machen und
                                    anzuzeigen, wie viele Besucher noch vor Dir an der Reihe sind.
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                            <ExpansionPanel>
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                    Gibt es auch eine App von mein-wartezimmer.de
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>Die ist gerade noch in Arbeit ;)</ExpansionPanelDetails>
                            </ExpansionPanel>
                            <ExpansionPanel>
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                    Gibt es die Möglichkeit zu sehen, welche Einrichtungen bereits mit
                                    mein-wartezimmer.de arbeiten?
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    Du kannst über die Möglichkeit eine Wartenummer zu ziehen, die Einrichtungen finden,
                                    die dieses Verfahren benutzen. Einrichtungen die nur persönlich, also z.B. per
                                    Telefon, Wartenummern vergebene kannst du nicht sehen. Wende dich aber einfach an
                                    deine Einrichtung für Details.
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                            <ExpansionPanel>
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                    Mit wie viel Vorlaufzeit soll ich erscheinen?
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    Früher als zum angegeben Zeitpunkt zu erscheinen ist nicht notwendig. Bitte sei aber
                                    pünktlich, um die Terminkoordination nicht durcheinander zu bringen.
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                        </div>
                    ) : (
                        <div className={"col-md-12 mb-5"}>
                            <h4>Einrichtung</h4>
                            <ExpansionPanel>
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                    Set-Up Deines Accounts
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    <ol>
                                        <li>
                                            Gehe zum{" "}
                                            <Link href={"/login"}>
                                                <a>Login</a>
                                            </Link>
                                        </li>
                                        <li>Registriere deine Einrichtung und deine Kontaktdaten ein.</li>
                                        <li>
                                            Trage die durchschnittliche Zeit ein, die ihr in deiner Einrichtung pro
                                            Besucher benötigt.
                                        </li>
                                        <li>
                                            Wenn Ihr verschiedene operative Einheiten habt: Ordne jeder Einheit einen
                                            virtuellen Wartebereich zu.
                                        </li>
                                    </ol>
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                            <ExpansionPanel>
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                    Tägliche Benutzung von mein-wartezimmer.de
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    <ol>
                                        <li>
                                            Entscheide dich ob du die Wartenummern für deine Einrichtung nur persönlich
                                            (z.B. per Telefon oder durch die Anmeldung bei dir) vergeben möchtest oder
                                            ob du die Wartenummern automatisch vergeben lassen möchtest. Bei der
                                            automatischen Variante kann sich ein Besucher selbst eine Wartenummer
                                            ausstellen lassen und deine Einrichtung ist über unser System auffindbar.
                                        </li>
                                        <li>
                                            Kommuniziere deine Wartenummer und mein-wartezimmer.de an deine Besucher
                                            <br />
                                            <br />
                                            <b>Für deinen Besucher (persönliche Vergabe):</b>
                                            <ol type={"a"}>
                                                <li>
                                                    Wir arbeiten mit mein-wartezimmer.de um es dir zu erleichtern, deine
                                                    Wartezeit besser einzuteilen, sie für dich transparenter zu
                                                    gestalten.
                                                </li>
                                                <li>
                                                    Bitte benutze dafür mein-wartezimmer.de und registriere dich mit
                                                    deiner ganz persönlichen Wartenummer - hier siehst du, wie lange es
                                                    noch dauern wird, wie viele Besucher vor dir sind und ob
                                                    gegebenenfalls ein Notfall vorgezogen wird
                                                </li>
                                            </ol>
                                            <br />
                                            <br />
                                            <b>Für deinen Besucher (automatische Vergabe):</b>
                                            <ol type={"a"}>
                                                <li>
                                                    Wir arbeiten mit mein-wartezimmer.de um es dir zu erleichtern, deine
                                                    Wartezeit besser einzuteilen, sie für dich transparenter zu
                                                    gestalten.
                                                </li>
                                                <li>
                                                    Bitte suche unsere Einrichtung auf mein-wartezimmer.de/organizations
                                                </li>
                                            </ol>
                                            <br />
                                            <br />
                                        </li>
                                        <li>Termine verschieben bei Notfallpatienten</li>
                                        <li>Überblick über die einzelnen Warteschlangen verschaffen</li>
                                    </ol>
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                        </div>
                    )}
                </div>
            </div>
        </Page>
    );
};

export default Help;
