import React, { useContext, useState } from "react";
import Page from "../layout/Page";
import LoyaltyTwoToneIcon from "@material-ui/icons/LoyaltyTwoTone";
import { Button, FormControlLabel, IconButton, InputAdornment, Switch, TextField } from "@material-ui/core";
import TabPanel from "../components/TabPanel";
import Dots from "../components/Dots";
import TabContainer from "../components/TabContainer";
import AddCircle from "@material-ui/icons/AddCircle";
import HighlightOffOutlinedIcon from "@material-ui/icons/HighlightOffOutlined";
import Utils from "../statics/Utils";
import { useInput } from "../hooks/useInput";
import { Validator } from "../statics/Validator";
import { AuthService } from "../statics/AuthService";
import { toast } from "react-toastify";
import { AppContext } from "../context/AppContext";
import { useRouter } from "next/router";
import AddressInput from "../components/AddressInput";
import { OpenStreetMapResponse } from "../shared";

const Register = () => {
    const { dispatch } = useContext(AppContext);
    const router = useRouter();
    const [sections, setSections] = useState<{ value: string; key: string }[]>([{ value: "", key: Utils.uniqId() }]);
    const [activeSlide, setActiveSlide] = useState(0);
    const email = useInput({ validator: Validator.isEmail });
    const [publicAccessible, setPublicAccessible] = useState(false);
    const email1 = useInput({ validator: (value: string) => Validator.isSame(value, email.value) });
    const password = useInput({ validator: Validator.password });
    const password1 = useInput({
        validator: (value: string) => Validator.isSame(value, password.value, true),
    });

    const [address, setAddress] = useState<OpenStreetMapResponse>();
    const title = useInput({ validator: Validator.notEmpty });
    const phoneNumber = useInput({ validator: Validator.notEmpty });
    const averageTimeNeeded = useInput<number>({ validator: Validator.notEmpty });
    const leadTime = useInput<number>();

    const handleRegister = async () => {
        if (!sections.length || (sections.length === 1 && sections[0].value.length === 0)) {
            return toast.warn("Bitte erstelle mindestens einen Raum / Abteilung / ... der Praxis");
        }
        if (!address) {
            return toast.warn("Bitte trage eine Adresse ein");
        }
        try {
            const syncResponse = await AuthService.register({
                ...address.address,
                title: title.value,
                address: address.display_name,
                lat: address.lat,
                lon: address.lon,
                phoneNumber: phoneNumber.value,
                averageTimeNeeded: averageTimeNeeded.value,
                password: password.value,
                email: email.value,
                publicAccessible,
                sections: sections.map(({ value }) => value),
            });
            dispatch({ type: "SUCCESSFUL_SYNC", payload: syncResponse });
            toast.success("Registrierung erfolgreich!");
            router.push("/protected/[organization]", `/protected/${syncResponse.slug}`);
        } catch (e) {
            if (e.response && e.response.status === 409) {
                toast.error("Ein Account mit dieser E-Mail Adresse ist bereits registriert");
            }
        }
    };
    const handleButtonClick = () => {
        if (activeSlide < 2) {
            return setActiveSlide(prevState => prevState + 1);
        }
        handleRegister();
    };
    const handleBackButtonClick = () => {
        if (activeSlide > 0) {
            setActiveSlide(prevState => prevState - 1);
        }
    };

    const handleAddButtonClick = () => {
        setSections(prevState => [...prevState, { value: "", key: Utils.uniqId() }]);
    };

    const handleSectionChange = (e, index) => {
        setSections(prevState => {
            const newState = [...prevState];
            newState[index].value = e.target.value;
            return newState;
        });
    };

    const handleDeleteSection = (key: string) => {
        const clone = [...sections];
        setSections(clone.filter(section => section.key !== key));
    };

    return (
        <Page
            description={"Melde dich mit deinen Praxis-Profil an um mit der Ausgabe von Wartenummern zu starten."}
            title={"Registrierung"}>
            <div className={"container pb-5 d-flex align-items-center justify-content-center"}>
                <form
                    className={"d-flex login-form card justify-content-center flex-column w-100"}
                    autoComplete={"off"}>
                    <h3 className={"text-center my-5"}>
                        <LoyaltyTwoToneIcon color={"error"} fontSize={"large"} /> Mein Wartezimmer
                    </h3>
                    <TabContainer active={activeSlide}>
                        <TabPanel>
                            <h4 className={"mb-4"}>Einstellungen</h4>
                            <FormControlLabel
                                control={
                                    <Switch
                                        className={"ml-2"}
                                        color={"primary"}
                                        checked={publicAccessible}
                                        onChange={() => setPublicAccessible(prevState => !prevState)}
                                        name="publicAccessible"
                                    />
                                }
                                label="Nutzer können Wartenummern generieren"
                            />
                            <h4 className={"mb-4"}>Login Informationen</h4>
                            <div className={"d-flex flex-column"}>
                                <TextField {...email} className={"mb-4"} variant={"outlined"} label={"Email"} />
                                <TextField {...email1} className={"mb-4"} variant={"outlined"} label={"Email Wdh."} />
                                <TextField
                                    type={"password"}
                                    {...password}
                                    className={"mb-4"}
                                    variant={"outlined"}
                                    helperText={
                                        "Das Passwort muss aus mindestens 6 Zeichen bestehen, Groß - und Kleinbuchstaben wie Zahlen enthalten"
                                    }
                                    label={"Passwort"}
                                />
                                <TextField
                                    type={"password"}
                                    {...password1}
                                    className={"mb-4"}
                                    variant={"outlined"}
                                    label={"Passwort Wdh."}
                                />
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <h4 className={"mb-4"}>Allgemeine Informationen</h4>
                            <div className={"d-flex flex-column"}>
                                <TextField {...title} className={"mb-4"} variant={"outlined"} label={"Name"} />
                                <AddressInput selected={address} onSelect={setAddress} className={"mb-4"} />
                                <TextField
                                    {...phoneNumber}
                                    type={"tel"}
                                    className={"mb-4"}
                                    variant={"outlined"}
                                    label={"Telefonnummer"}
                                />
                            </div>
                            <div className={"row"}>
                                <div className={"col-md-6"}>
                                    <TextField
                                        {...averageTimeNeeded}
                                        type={"number"}
                                        className={"w-100"}
                                        variant={"outlined"}
                                        label={"Durschnittl. Wartezeit in Minuten"}
                                    />
                                </div>
                                <div className={"col-md-6"}>
                                    <TextField
                                        {...leadTime}
                                        type={"number"}
                                        className={"w-100"}
                                        variant={"outlined"}
                                        label={"Vorlaufzeit in Minuten (optional)"}
                                    />
                                </div>
                            </div>
                            <div className={"row"}>
                                <div className={"col-md-12"}>
                                    <small>
                                        Trage die durchschnittliche Zeit ein, die ihr in deiner Einrichtung pro Besucher
                                        benötigt. Die Vorlaufzeit ist optional, du kannst sie also frei lassen. Wenn die
                                        durchschnittliche Wartezeit lang ist, kannst du eine kürzere Vorlaufzeit
                                        eintragen. Diese wird dann zur genutzt, damit deine Besucher passgenau da sind.
                                    </small>
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <h4 className={"mb-4"}>Jetzt ist Struktur gefragt!</h4>
                            <p>
                                Eure Einrichtung hat mehrere Räume, Abteilungen oder Bereiche? Hier kannst du all das
                                eintragen (und natürlich später im Praxis-Profil bearbeiten)!
                            </p>
                            {sections.map(({ key, value }, index) => (
                                <div className={"w-100"} key={index}>
                                    <TextField
                                        variant={"outlined"}
                                        key={key}
                                        className={"mb-4 w-100"}
                                        label={"Raum / Abteilung / ..."}
                                        value={value}
                                        InputLabelProps={{ shrink: value.length > 0 }}
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position={"end"}>
                                                    <IconButton onClick={() => handleDeleteSection(key)}>
                                                        <HighlightOffOutlinedIcon fontSize={"large"} />
                                                    </IconButton>
                                                </InputAdornment>
                                            ),
                                        }}
                                        onChange={e => {
                                            e.persist();
                                            handleSectionChange(e, index);
                                        }}
                                    />
                                </div>
                            ))}
                            <div className={"d-flex justify-content-end"}>
                                <IconButton onClick={handleAddButtonClick} color={"secondary"}>
                                    <AddCircle fontSize={"large"} />
                                </IconButton>
                            </div>
                        </TabPanel>
                    </TabContainer>
                    <div className={"d-flex w-100 justify-content-center align-items-center"}>
                        <Dots className={"position-absolute"} amount={3} active={activeSlide} />
                        <div className={"ml-auto"}>
                            {activeSlide > 0 && (
                                <Button
                                    onClick={handleBackButtonClick}
                                    color={"secondary"}
                                    variant={"contained"}
                                    className={"mr-3"}
                                    disableElevation={true}>
                                    Zurück
                                </Button>
                            )}
                            <Button
                                disabled={
                                    activeSlide === 2 &&
                                    (email1.error ||
                                        email.error ||
                                        !address ||
                                        title.error ||
                                        password.error ||
                                        sections.length < 1 ||
                                        password1.error)
                                }
                                onClick={handleButtonClick}
                                color={"primary"}
                                variant={"contained"}
                                disableElevation={true}>
                                {activeSlide === 2 ? "Registrieren" : "Weiter"}
                            </Button>
                        </div>
                    </div>
                </form>
            </div>
        </Page>
    );
};

export default Register;
