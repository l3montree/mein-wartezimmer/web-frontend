import React from "react";
import Page from "../layout/Page";

const Imprint = () => {
    return (
        <Page title={"Impressum"} description={""}>
            <div className={"container"}>
                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <h2 className={"pb-3"}>Impressum</h2>
                        <h3>Angaben gemäß § 5 TMG</h3>
                        Tim Bastin & Sebastian Kawelke
                        <br />
                        In der Grächt 27
                        <br />
                        53127 Bonn
                        <br />
                        <br />
                        <p>
                            Diese Seite wurde entwickelt von:
                            <ul>
                                <li>Tim Bastin</li>
                                <li>Nadine Butzhammer</li>
                                <li>Cristina Ostler</li>
                                <li>Sebastian Kawelke</li>
                            </ul>
                        </p>
                        <p>
                            <h3>Kontakt</h3>
                            E-Mail: <a href={"mailto:info@mein-wartezimmer.de"}>info@mein-wartezimmer.de</a>
                        </p>
                        <h3>Haftung für Inhalte</h3>
                        <p>
                            Die Inhalte unserer Seiten wurden mit größter Sorgfalt erstellt. Für die Richtigkeit,
                            Vollständigkeit und Aktualität der Inhalte können wir jedoch keine Gewähr übernehmen. Als
                            Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den
                            allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter
                            jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen
                            oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen.
                            Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den
                            allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst
                            ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden
                            von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.
                        </p>
                        <h3>Haftung für Links</h3>
                        <p>
                            Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen
                            Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen.
                            Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der
                            Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf
                            mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung
                            nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne
                            konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von
                            Rechtsverletzungen werden wir derartige Links umgehend entfernen.
                        </p>
                        <h3>Open Source</h3>
                        <p>
                            Dieser Auftritt und seine Ressourcen (Code, etc) stehen unter einer Open Source Lizenz und
                            stehen zur persönlichen und kommerziellen Nutzung frei zur Verfügung.
                        </p>
                        <hr />
                        <p>
                            Icons (verwendet in Video und in Info Grafiken) erstellt von Freepik, Vectors Market und
                            DinosoftLabs von <a href={"flaticon.com"}>flaticon.com</a>
                        </p>
                        <p>Soundtrack im Video (Upbeat Party) erstellt von Scott Holmes (Lizenz: (CC BY-NC 4.0))</p>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default Imprint;
