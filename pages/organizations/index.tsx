import React, { useContext, useRef, useState } from "react";
import Page from "../../layout/Page";
import Form from "../../components/Form";
import Animation from "../../components/Animation";
import searchAnimation from "../../animations/search.json";
import { useInput } from "../../hooks/useInput";
import { debounce } from "lodash";
import OrganizationInput from "../../components/OrganizationInput";
import Organization from "../../api-layer/Organization";
import { Organization as SharedOrganization } from "../../shared";
import dynamic from "next/dynamic";
import { MapProps, MarkerProps, TileLayerProps, TooltipProps } from "react-leaflet";
import { Section } from "../../shared/auth";
import DrawTicketDialog from "../../components/DrawTicketDialog";
import Ticket from "../../api-layer/Ticket";
import { AppContext } from "../../context/AppContext";
import { toast } from "react-toastify";
import { useRouter } from "next/router";

const [Map, TileLayer, Marker, Tooltip] = [
    dynamic<MapProps>(() => import("react-leaflet").then(mod => mod.Map), { ssr: false }),
    dynamic<TileLayerProps>(() => import("react-leaflet").then(mod => mod.TileLayer), { ssr: false }),
    dynamic<MarkerProps>(() => import("react-leaflet").then(mod => mod.Marker), { ssr: false }),
    dynamic<TooltipProps>(() => import("react-leaflet").then(mod => mod.Tooltip), { ssr: false }),
];

const Index = () => {
    const input = useInput();
    const router = useRouter();
    const { dispatch } = useContext(AppContext);
    const [selected, setSelected] = useState<SharedOrganization & { __sections__: Section[] }>();
    const [options, setOptions] = useState<Array<SharedOrganization & { __sections__: Section[] }>>([]);

    const query = useRef<(q) => void>(
        debounce(async q => {
            const res = await Organization.search(q);
            setOptions(res);
        }, 300),
    ).current;

    const handleChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
        const q = e.target.value;
        if (q.length > 3) {
            query(q);
        }
        input.onChange(e);
    };

    const handleSelect = (org: SharedOrganization & { __sections__: Section[] }) => {
        if (typeof org === "object") {
            setSelected(org);
        }
    };

    const handleDraw = async (values: { sectionId: number; title: string }) => {
        const res = await new Ticket(selected.id).storePublic(values);
        dispatch({ type: "SAVE_TICKET", payload: res });
        dispatch({
            type: "SET_TICKET",
            payload: { currentTime: new Date(), ...(await Ticket.get(res.id.toString())) },
        });
        setSelected(null);
        router.push(`/?ticketId=${res.id}`);
        toast.success("Wartenummer erfolgreich gezogen");
    };

    return (
        <Page
            description={
                "Generiere eine Wartenummer bei einer teilnehmenden Einrichtung. Nutze die Karte um dich schnell zurecht zu finden. Komme immer pünktlich."
            }
            title={"Wartenummer generieren"}>
            <DrawTicketDialog onDraw={handleDraw} open={!!selected} org={selected} onClose={() => setSelected(null)} />
            <div className={"container min-height-100"}>
                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <form
                            onSubmit={e => e.preventDefault()}
                            className={"d-flex justify-content-center flex-column w-100"}
                            autoComplete={"off"}>
                            <Animation
                                className={"heart-animation search-animation mb-5"}
                                animationData={searchAnimation}
                                loop={true}
                                autoplay={true}
                            />
                            <h2 className={"pb-3 font-weight-bold text-center"}>
                                Nach einer teilnehmenden Einrichtung suchen
                            </h2>
                            <OrganizationInput
                                options={options}
                                onSelect={(_, org) => handleSelect(org)}
                                onChange={handleChange}
                            />
                        </form>
                        <Map
                            className={"my-4"}
                            center={options.length >= 1 ? [+options[0].lat, +options[0].lon] : [50.737431, 7.098207]}
                            zoom={13}>
                            <TileLayer
                                className={"leaflet-tiles"}
                                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                            />
                            {options.map(org => (
                                // @ts-ignore
                                <Marker key={org.id} onClick={() => handleSelect(org)} position={[+org.lat, +org.lon]}>
                                    <Tooltip className={"org-tooltip"} permanent={true}>
                                        {org.title}
                                    </Tooltip>
                                </Marker>
                            ))}
                        </Map>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default Index;
