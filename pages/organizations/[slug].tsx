import React, { useContext, useEffect, useState } from "react";
import Page from "../../layout/Page";
import { useRouter } from "next/router";
import { Organization as SharedOrganization } from "../../shared";
import { Section } from "../../shared/auth";
import Organization from "../../api-layer/Organization";
import Page404 from "../404";
import Loading from "../../components/Loading";
import OrganizationInfo from "../../components/OrganizationInfo";
import dynamic from "next/dynamic";
import { MapProps, MarkerProps, TileLayerProps, TooltipProps } from "react-leaflet";
import Link from "next/link";
import { Button } from "@material-ui/core";
import DrawTicketDialog from "../../components/DrawTicketDialog";
import Ticket from "../../api-layer/Ticket";
import { toast } from "react-toastify";
import { AppContext } from "../../context/AppContext";

const [Map, TileLayer, Marker, Tooltip] = [
    dynamic<MapProps>(() => import("react-leaflet").then(mod => mod.Map), { ssr: false }),
    dynamic<TileLayerProps>(() => import("react-leaflet").then(mod => mod.TileLayer), { ssr: false }),
    dynamic<MarkerProps>(() => import("react-leaflet").then(mod => mod.Marker), { ssr: false }),
    dynamic<TooltipProps>(() => import("react-leaflet").then(mod => mod.Tooltip), { ssr: false }),
];

const OrganizationDetail = () => {
    const router = useRouter();
    const { dispatch } = useContext(AppContext);
    const [open, setOpen] = useState(false);
    const [organization, set] = useState<SharedOrganization & { __sections__: Section[] }>(null);
    const [loading, setLoading] = useState(true);
    const { slug } = router.query;
    useEffect(() => {
        const fetchData = async () => {
            try {
                const res = await Organization.getBySlug(slug as string);
                set(res);
            } catch (e) {
                // silence - organization will stay null, therefore we are displaying 404.
            } finally {
                setLoading(false);
            }
        };
        fetchData();
    }, [slug]);
    if (loading) {
        return <Loading />;
    } else if (!organization) {
        return <Page404 />;
    }

    const handleDraw = async (values: { sectionId: number; title: string }) => {
        const res = await new Ticket(organization.id).storePublic(values);
        dispatch({ type: "SAVE_TICKET", payload: res });
        dispatch({
            type: "SET_TICKET",
            payload: { currentTime: new Date(), ...(await Ticket.get(res.id.toString())) },
        });
        router.push("/");
        toast.success("Wartenummer erfolgreich gezogen");
    };

    return (
        <Page
            description={`${organization.title}- Ziehe eine Wartenummer um deine Wartezeit transparenter zu gestalten.`}
            title={organization.title}>
            <DrawTicketDialog onDraw={handleDraw} open={open} org={organization} onClose={() => setOpen(false)} />
            <div className={"container"}>
                <div className={"row mb-4"}>
                    <div className={"col-md-12"}>
                        <Link href={"/"}>
                            <a>Start</a>
                        </Link>
                        <span className={"px-2"}>/</span>
                        <Link href={"/organizations"}>
                            <a>Einrichtung suchen</a>
                        </Link>
                        <span className={"px-2"}>/</span>
                        <span>{organization.title}</span>
                    </div>
                </div>
                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <h1>{organization.title}</h1>
                    </div>
                </div>
                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <OrganizationInfo {...organization} />
                        <Button
                            onClick={() => setOpen(true)}
                            disableElevation={true}
                            variant={"contained"}
                            color={"secondary"}>
                            Wartenummer ziehen
                        </Button>
                    </div>
                </div>
                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <Map className={"my-4"} center={[+organization.lat, +organization.lon]} zoom={13}>
                            <TileLayer
                                className={"leaflet-tiles"}
                                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                            />
                            <Marker key={organization.id} position={[+organization.lat, +organization.lon]}>
                                <Tooltip className={"org-tooltip"} permanent={true}>
                                    {organization.title}
                                </Tooltip>
                            </Marker>
                        </Map>
                    </div>
                </div>
            </div>
        </Page>
    );
};

export default OrganizationDetail;
