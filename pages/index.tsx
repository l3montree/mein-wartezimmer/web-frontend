import Page from "../layout/Page";
import { Button, TextField } from "@material-ui/core";
import Animation from "../components/Animation";
import home from "../animations/home1.json";
import React, { useContext, useEffect, useRef } from "react";
import LoyaltyTwoToneIcon from "@material-ui/icons/LoyaltyTwoTone";
import { useInput } from "../hooks/useInput";
import Ticket from "../api-layer/Ticket";
import { toast } from "react-toastify";
import TicketInfoContainer from "../components/TicketInfoContainer";
import Form from "../components/Form";
import { useRouter } from "next/router";
import { AppContext } from "../context/AppContext";

const Index = () => {
    const ticketId = useInput<string>();
    const { activeTicket, dispatch } = useContext(AppContext);
    const interval = useRef(null);
    const router = useRouter();
    const handleSend = (activeTicketId?: number) => {
        const fetch = async () => {
            try {
                const data = await Ticket.get(activeTicketId ? activeTicketId.toString() : ticketId.value);
                dispatch({ type: "SET_TICKET", payload: { ...data, currentTime: new Date() } });
            } catch (e) {
                clearInterval(interval.current);
                toast.error("Wir konnten deine Wartenummer nicht finden");
            }
        };
        interval.current = setInterval(() => {
            fetch();
        }, 10000);
        fetch();
    };
    useEffect(() => {
        if (activeTicket) {
            handleSend(activeTicket.id);
        } else if ("ticketId" in router.query) {
            handleSend(+router.query.ticketId);
        }
        return () => clearInterval(interval.current);
    }, []);

    const handleClear = () => {
        dispatch({ type: "CLEAR_TICKET" });
        clearInterval(interval.current);
    };

    return (
        <Page
            description={
                "Trage deine Wartenummer für dein digitales Wartezimmer ein. Du siehst wie lange du noch warten musst und kannst deine Zeit besser einteilen."
            }
            title={"Ticker"}>
            {activeTicket ? (
                <TicketInfoContainer onClear={handleClear} {...activeTicket} />
            ) : (
                <div className={"container min-height-100 d-flex align-items-center justify-content-center"}>
                    <div className={"w-100"}>
                        <Form
                            onSubmit={handleSend}
                            className={"d-flex justify-content-center flex-column w-100 index-form"}
                            autoComplete={"off"}>
                            <Animation
                                className={"heart-animation heart-animation-index mb-5"}
                                animationData={home}
                                loop={true}
                                autoplay={true}
                            />
                            <h2 className={"pb-3 font-weight-bold text-center"}>Deine Wartenummer eintragen</h2>
                            <TextField
                                {...ticketId}
                                helperText={
                                    "Trage hier deine Wartenummer ein, die du von deiner Einrichtung bekommen hast."
                                }
                                label={"Wartenummer"}
                                className={"mb-3"}
                                variant="outlined"
                            />
                            <Button
                                className={"align-self-end"}
                                variant={"contained"}
                                color={"primary"}
                                type={"submit"}
                                disableElevation={true}>
                                Senden
                            </Button>
                        </Form>
                        <div className={"my-4 w-100 d-flex align-items-center justify-content-center"}>
                            <div className={"border-div mr-5"} />
                            <b>Oder</b>
                            <div className={"border-div ml-5"} />
                        </div>
                        <Button
                            onClick={() => router.push("/organizations")}
                            className={"w-100"}
                            color={"secondary"}
                            variant={"contained"}
                            disableElevation={true}>
                            Wartenummer ziehen
                        </Button>
                    </div>
                </div>
            )}

            <section className={"mt-5 bg-danger text-light position-relative"}>
                <svg
                    className="polygon d-none d-md-block"
                    fill="#f8f9fa"
                    version="1.1"
                    x="0px"
                    y="0px"
                    viewBox="0 0 224.7 5.1">
                    <polygon points="0,0 224.7,0 224.7,5.1 0,1.5 "></polygon>
                </svg>
                <div className={"container pt-5"}>
                    <div className={"row mb-5"}>
                        <div className={"col-md-12"}>
                            <h2>
                                <LoyaltyTwoToneIcon className={"mr-2"} fontSize={"large"} />
                                Warum? Wer? Wie? - Das Wichtigste im Überblick
                            </h2>
                        </div>
                    </div>
                    <div className={"row mb-5"}>
                        <div className={"col-md-7"}>
                            <video controls={true} className={"w-100"} poster={"/img/poster.png"}>
                                <source src="/videos/landing.mp4" type="video/mp4" />
                            </video>
                        </div>
                        <div className={"col-md-5"}>
                            <p>
                                In einem vollen Wartezimmer oder stundenlang in einer Schlange hält sich heutzutage wohl
                                niemand gerne auf… Vor allem in der aktuellen Situation sind viele betroffen. Sei es
                                beim Arztbesuch oder bei der Einlassregulation in Supermärkten.
                            </p>
                            <p>
                                Dieses transparente Ticketsystem entlastet die Wartenden, indem diese ihre Wartezeit
                                z.B. auch Zuhause verbringen können. Das verringert zum einen das Risiko von
                                Krankheitsübertragungen und zum Anderen kann die gewonnene Zeit ja auch wesentlich
                                besser genutzt werden, beispielsweise mit einer heißen Hühnersuppe auf der Couch.
                            </p>
                            <p>
                                Die Einrichtung kann wahlweise direkt (z.B. im Terminvergabe-Gespräch) Wartenummern
                                vergeben oder lässt die Besucher, Kunden, Patienten, etc. bei Bedarf selbst eine
                                Wartenummer generieren. Diese geben sie im Anschluss auf mein-wartezimmer.de ein und
                                erkennen sofort wann sie an der Reihe sind und sich auf den Weg machen können.
                            </p>
                        </div>
                    </div>
                </div>
                <svg
                    style={{ transform: "rotateY(180deg) translateY(98%)", bottom: 0 }}
                    className="polygon d-none d-md-block text-danger"
                    version="1.1"
                    x="0px"
                    y="0px"
                    viewBox="0 0 224.7 5.1">
                    <polygon fill={"currentColor"} points="0,0 224.7,0 224.7,5.1 0,1.5 "></polygon>
                </svg>
            </section>
            <section className={"py-5"}>
                <div className={"container"}>
                    <div className={"row pt-5 mb-5"}>
                        <div className={"col-md-12"}>
                            <h2>Offen, kostenlos und für die Gesellschaft</h2>
                        </div>
                    </div>
                    <div className={"row"}>
                        <div className={"col-md-5"}>
                            <h5>Der Anlass</h5>
                            <p>
                                mein-wartezimmer.de ist in 48h im Rahmen des{" "}
                                <a rel="noreferrer" target={"_blank"} href={"https://twitter.com/WirvsVirusHack"}>
                                    #WirVsVirusHackathons
                                </a>{" "}
                                der Bundesregierung entstanden.
                            </p>
                            <h5>Die Aufgabe</h5>
                            <p>
                                Herausforderungen aus der Gesellschaft und der Bundesregierung, ausgelöst durch die
                                Corona-Krise, zu lösen.
                            </p>
                            <h5>Die Ressourcen</h5>
                            <p>Eine Plattform und die Kraft und Motivation, die jeder mitgebracht hat.</p>
                            <h5>Unsere Mission</h5>
                            <p>
                                Für unser Team (Tim Bastin, Nadine Butzhammer, Cristina Ostler, Sebastian Kawelke) war
                                schnell klar, dass wir an der Herausforderung der Minimierung des Ansteckungsrisikos in
                                Arztpraxen arbeiten wollten - 48h später war das Tool mein-wartezimmer.de geboren.
                            </p>
                        </div>
                        <div className={"col-md-7"}>
                            <img alt={"WirVsVirus Logo"} className={"wirvsvirus"} src={"/img/Logo_Projekt_01.png"} />
                        </div>
                    </div>
                </div>
            </section>
        </Page>
    );
};

export default Index;
