# Web Frontend

### Allgemeines
#### Allgemeine Anfroderungen
##### Technische Anforderungen
*  Vorgaben durch TMG und DSGVO müssen eingehalten werden
*  Cookies nur wenn absolut notwendig (kein Tracking, keine Statistik, etc. (Opt in, default false))
*  "Desto Weniger, desto besser" - Grundsatz
*  

### Verwendung


### Weitere Dokumentationen

##### UX & UI
Für die Benutzerführung und das Design des Web Frontends findets du [hier](https://gitlab.com/l3montree/wartezimmer-tracker/web-frontend/-/wikis/User-Interface-&-User-Actions) eine Wiki Page.

##### Benutzerhandbuch
| Benutzertyp | Link zum Handbuch |
|-------------|-------------------|
|       User/Besucher      |     [hier](https://gitlab.com/l3montree/wartezimmer-tracker/web-frontend/-/wikis/Benutzerhandbuch-f%C3%BCr-Mager-&-Praxen)              |
|      Manager/Praxis       |       [hier](https://gitlab.com/l3montree/wartezimmer-tracker/mobile-app/-/wikis/Benutzerhandbuch-f%C3%BCr-User-&-Besucheren)            |


### Installation

#### Requirements
1. nodejs / npm [https://nodejs.org/en/download/](https://nodejs.org/en/download/)
#### Repository Clonen
```shell script
git clone https://gitlab.com/l3montree/wartezimmer-tracker/web-frontend.git
```

#### Abhängigkeiten installieren
````shell script
cd web-frontend
npm install
````

#### Starten der App
Das Starten der App ist auf unterschiedlichem Weg möglich:

1. Während der Entwicklung:
    ```shell script
    npm run dev
    ```
2. In der Produktiv-Umgebung:
    ```shell script
    npm run start
    ```
