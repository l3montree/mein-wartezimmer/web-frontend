import { Ticket as SharedTicket } from "../shared";

export default class Storage {
    static saveLocalTickets(tickets: SharedTicket[]) {
        localStorage.setItem("tickets", JSON.stringify(tickets));
    }

    static getLocalTickets(): SharedTicket[] {
        const tickets = localStorage.getItem("tickets");
        if (tickets) {
            return JSON.parse(tickets);
        }
        return [];
    }
}
