export class Validator {
    static notEmpty(value?: number);
    static notEmpty(value?: string);
    static notEmpty(value: string | number | undefined) {
        if (typeof value === "number") {
            return value > 0;
        } else if (value) {
            return value.length >= 1;
        }
        return false;
    }

    static isEmail(value?: string) {
        return new RegExp(
            /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        ).test(value);
    }

    static isSame(value?: string, compare?: string, strict?: boolean) {
        if (strict) {
            return value === compare;
        } else if (value && compare) {
            return value.toLowerCase() === compare.toLowerCase();
        }
        return false;
    }

    static password(value?: string) {
        return new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})").test(value);
    }
}
