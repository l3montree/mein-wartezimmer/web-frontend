import API, { PublicAPI } from "./API";
import { AxiosResponse } from "axios";
import User from "../api-layer/Organization";
import Token, { IToken } from "../api-layer/Token";
import Application from "./Application";
import { AuthRoutes } from "../shared/routes";
import Organization from "../api-layer/Organization";

export class AuthService {
    /**
     * Please use convenient method @user instead.
     * Keep this private :-)
     */
    private static current: User | undefined;

    /**
     * Log the user in.
     * Fetches the user afterwards.
     * @param email
     * @param password
     */
    public static async login(email: string, password: string): Promise<AuthRoutes["SYNC"]["response"]> {
        // gets the new token
        await this.loginWithCredentials(email, password);
        return this.sync();
    }

    /**
     * Try to register the user.
     * Logs him in afterwards.
     * @param registerRequest
     */
    public static async register(
        registerRequest: AuthRoutes["SIGNUP"]["request"],
    ): Promise<AuthRoutes["SYNC"]["response"]> {
        const { data }: AxiosResponse<AuthRoutes["SIGNUP"]["response"]> = await PublicAPI.post(
            `auth/signup`,
            registerRequest,
        );
        new Token(data.accessToken, data.refreshToken).save();
        Application.installNetworkInstance();
        // we should be able to fetch the current user right now.
        return this.sync();
    }
    /**
     * Get the current user.
     * This method is just a convenient method, which makes it possible to strictly type
     * the user object, without the need of any assertions.
     */
    public static org(): Organization {
        if (this.current) {
            return this.current;
        } else {
            throw new Error("User not saved in AuthService");
        }
    }

    /**
     * Try to log the user in using his credentials.
     * Might fail.
     * PublicAPI is used - calling auth route.
     * Installs the network instances on success.
     * @param email
     * @param password
     */
    private static async loginWithCredentials(email: string, password: string) {
        const { data } = await PublicAPI.post<AuthRoutes["LOGIN"]["response"]>("auth/login", { email, password });
        new Token(data.accessToken, data.refreshToken).save();
        // install the network instance after successful login.
        Application.installNetworkInstance();
    }

    /**
     * Get the current user information.
     * Sync server and client. Needed to initially load user data.
     * Saves the current user as class property.
     */
    public static async sync(): Promise<AuthRoutes["SYNC"]["response"]> {
        const { data } = await API.get<AuthRoutes["SYNC"]["response"]>("auth/sync");
        AuthService.current = new Organization();
        // user is still logged in
        return data;
    }

    /**
     * Get a fresh token from the database using the refresh token.
     * Installs network instances after saving tokens.
     */
    public static async refreshToken() {
        const { data: token } = await PublicAPI.post<IToken>("auth/refresh", {
            refreshToken: Token.getRefreshToken(),
        });
        new Token(token.accessToken, token.refreshToken).save();
        // install network instance after successful login.
        await Application.installNetworkInstance();
    }

    /**
     * Log the user out.
     */
    public static logout() {
        // destroy tokens
        // refresh tokens are getting deleted server-side.
        Token.destroy();
    }

    public static forgotPassword(email: string) {
        return PublicAPI.post("auth/forgot-password", { email });
    }

    public static async resetPassword(data: { token: string; password: string }) {
        const token = (await PublicAPI.post<IToken>("auth/reset-password", data)).data;
        new Token(token.accessToken, token.refreshToken).save();
        await Application.installNetworkInstance();
        // we should be able to fetch the current user right now.
        return this.sync();
    }
}
