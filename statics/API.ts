import axios, { AxiosError } from "axios";
import { Logger } from "./Logger";
import Application from "./Application";

/**
 * API instance, used for every auth API call.
 */
const API = axios.create({
    baseURL: process.env.API_HOST,
    // authorization header gets set during runtime.
});

// adding a response interceptor.
// whenever we encounter a 401 with the response body "JWT is expired", we will try to refresh.
// if refreshing is not possible, we redirect to login.
API.interceptors.response.use(
    response => response,
    async (error: AxiosError) => {
        // fast recovery path.
        Logger.log(error.response?.data);
        if (error.response?.status === 401 && error.response?.data === "JWT is expired") {
            await Application.refreshRoutine();
            return API.request(error.config);
        }
        return Promise.reject(error);
    },
);
/**
 * USED TO CALL AUTHORIZATION CONTROLLERS.
 */
export const PublicAPI = axios.create({
    baseURL: process.env.API_HOST,
});

export default API;
