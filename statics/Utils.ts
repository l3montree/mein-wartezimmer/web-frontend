import { v4 as uuidv4 } from "uuid";
import { Logger, SEVERITY } from "./Logger";
import { cloneDeep } from "lodash";

export default class Utils {
    public static uniqId(): string {
        return uuidv4();
    }

    public static reorder<T>(list: T[], startIndex: number, endIndex: number): T[] {
        const result: T[] = cloneDeep(list);
        const [removed] = result.splice(startIndex, 1);
        result.splice(endIndex, 0, removed);

        return result;
    }

    public static async safeInvoke<T>(
        promiseOrFunction: (() => T | Promise<T>) | Promise<T> | T,
        severity: SEVERITY = SEVERITY.LOW,
    ): Promise<T | false> {
        try {
            // check if the passed variable is a callable.
            if (promiseOrFunction instanceof Function) {
                return await promiseOrFunction();
            } else {
                // otherwise just return the value.
                return await promiseOrFunction;
            }
        } catch (e) {
            // on error call the logger.
            switch (severity) {
                case SEVERITY.LOW:
                    Logger.log(e);
                    break;
                case SEVERITY.MEDIUM:
                    Logger.warn(e);
                    break;
                case SEVERITY.HIGH:
                    Logger.warn(e);
                    break;
                case SEVERITY.CRITICAL:
                    Logger.error(e);
                    break;
                default:
                    Logger.log(e);
                    break;
            }
            return false;
        }
    }

    /**
     * Moves an item from one list to another list.
     */
    public static move(source, destination, droppableSource, droppableDestination) {
        const sourceClone = Array.from(source);
        const destClone = Array.from(destination);
        const [removed] = sourceClone.splice(droppableSource.index, 1);

        destClone.splice(droppableDestination.index, 0, removed);

        const result = {};
        result[droppableSource.droppableId] = sourceClone;
        result[droppableDestination.droppableId] = destClone;

        return result;
    }
}
