import API from "./API";
import Token from "../api-layer/Token";
import { AuthService } from "./AuthService";
import Utils from "./Utils";
import moment from "moment";
import "moment/locale/de";
import { AuthRoutes } from "../shared/routes";

export default class Application {
    /**
     * Boots the application.
     * Returns true if user is currently logged in
     * Returns false if user is not logged in.
     * Returns initial route
     */
    public static async boot(): Promise<AuthRoutes["SYNC"]["response"] | void> {
        moment.locale("de");
        // boot the Network instances
        // tries to fetch token from store.
        // using saveInvoke to log the error via the Logger object.
        const installationSuccessful = Application.installNetworkInstance();
        if (installationSuccessful) {
            // if the installation was successful we can start the authorization routine.
            // start the authorization routine on booting.
            return await Application.authRoutine();
        }
        // installation of network API instance was not successful.
        // the Application@authRoutine WILL ALWAYS FAIL.
        // silence.
    }

    public static installNetworkInstance(): true | false {
        const token = Token.get();
        if (token) {
            API.defaults.headers.authorization = `Bearer ${token.accessToken}`;
            return true;
        }
        // Token not found found in store
        return false;
    }

    private static async authRoutine(): Promise<AuthRoutes["SYNC"]["response"] | void> {
        // installation was successful.
        // token in store was found.
        // token is now set inside the Network API instance.

        // try to fetch the user.
        const fetchSuccessful = await Utils.safeInvoke(AuthService.sync);
        if (fetchSuccessful) {
            // user is now set in AuthService as current logged in user.
            return fetchSuccessful;
        } else {
            return await Application.refreshRoutine();
        }
    }

    public static async refreshRoutine(): Promise<AuthRoutes["SYNC"]["response"] | void> {
        // token is not valid anymore
        // try to refresh the current user.
        await Utils.safeInvoke(AuthService.refreshToken);
        return await AuthService.sync();
    }
}
