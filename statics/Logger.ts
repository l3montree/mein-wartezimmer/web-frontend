// Test wont work with import from sentry

interface ILogger {
    /**
     * Logs error messages.
     * @param error
     * @param severity
     */
    log: (message: any) => void;
    warn: (message: any) => void;
    error: (message: any) => void;
}

export enum SEVERITY {
    "LOW",
    "MEDIUM",
    "HIGH",
    "CRITICAL",
}
export const Logger: ILogger = {
    log: process.env.ENV !== "production" ? console.log : () => false, //  captureMessage,
    warn: process.env.ENV !== "production" ? console.warn : () => false, // captureMessage,
    error: process.env.ENV !== "production" ? console.error : () => false, // captureMessage,
};
