import { Dispatch, Reducer, ReducerAction, useEffect, useReducer, useRef } from "react";
import { Paginated } from "../shared";
import { IUseList, useList } from "./useList";
import { AxiosError } from "axios";

/**
 * Interface to strongly type the parameters of the useFetch hook.
 */
export interface IUseFetchInfiniteParam<T> {
    // Operation which fetches the data.
    operation: (param: { page: string }) => Promise<Paginated<T>>;
    // the initial state. - Default is undefined
    initial?: Paginated<T>;
    deps?: any[];
    interval?: number;
}

/**
 * Reducer of the useFetch hook.
 * handles the loading and error state as well as the data.
 * Provides an action to replace the current data.
 * @param {IState<T>} state
 * @param {Action<T>} action
 * @returns {IState<T>}
 */
const fetchReducer: Reducer<any, any> = <T>(state: IState<T>, action: Action<T>): IState<T> => {
    switch (action.type) {
        case "request":
            return {
                data: undefined,
                error: null,
                loading: true,
                refreshing: false,
            };
        case "error":
            return {
                data: undefined,
                loading: false,
                error: action.payload,
                refreshing: false,
            };
        case "success":
            return {
                data: action.payload,
                loading: false,
                error: null,
                refreshing: false,
            };
        case "loaded more":
            return {
                ...state,
                data: {
                    ...action.payload,
                    content: state.data?.content.concat(action.payload.content) || [],
                },
                loading: false,
            };
        case "UNCONVENTIONAL_replace":
            return {
                ...state,
                data: action.payload,
            };
        case "loading more":
            return {
                ...state,
                loading: true,
            };
        case "reset":
            return {
                loading: true,
                error: null,
                data: undefined,
                refreshing: false,
            };
        case "refresh":
            return {
                ...state,
                refreshing: true,
            };
        default:
            return state;
    }
};

/**
 * Return type of the useFetch hook
 */
export interface IUseFetchInfinite<T extends { id: any }> {
    // The real data - might be undefined before the operation is finished.
    data: IUseList<T> | undefined;
    // the loading state
    loading: boolean;
    // refreshing state
    refreshing: boolean;
    // the error state
    error: AxiosError | null;
    // load more data - will append the fetched data to the previously fetched data.
    loadMore: () => Promise<Paginated<T> | undefined>;
    // function to manually fetch the data again.
    fetchData: (operation?: () => Promise<Paginated<T>>) => Promise<Paginated<T> | undefined>;
    // refresh the data - fetchData but sets refreshing state
    refresh: (operation?: (param: { page: string }) => Promise<Paginated<T>>) => Promise<Paginated<T> | undefined>;
    // reducer dispatch function
    dispatch: Dispatch<ReducerAction<Reducer<IState<T>, Action<T>>>>;
    // reset the data source.
    reset: () => void;
}

/**
 * The strongly typed state of the hook.
 */
interface IState<T> {
    loading: boolean;
    refreshing: boolean;
    error: AxiosError | null;
    data: Paginated<T> | undefined;
}

/**
 * Possible actions.
 */
type Action<T> =
    | { type: "request" }
    | { type: "loaded more"; payload: Paginated<T> }
    | { type: "success"; payload: Paginated<T> }
    | { type: "error"; payload: AxiosError }
    | { type: "UNCONVENTIONAL_replace"; payload: Paginated<T> }
    | { type: "reset" }
    | { type: "loading more" }
    | { type: "refresh" };

/**
 * Use fetch hook.
 * Used to fetch data from an external API and set is in the state.
 * Handles loading as well as error state.
 * @param {IUseFetchParam<T>} config
 * @returns {IUseFetch<T>}
 */
export function useFetchInfinite<T extends { id: any }>(config: IUseFetchInfiniteParam<T>): IUseFetchInfinite<T> {
    const intervall = useRef(null);
    const [state, dispatch] = useReducer<Reducer<IState<T>, Action<T>>>(fetchReducer, {
        data: config.initial,
        loading: true,
        refreshing: false,
        error: null,
    });
    // dependencies of the hook
    // defaults to an empty array (gets called only on mount then)
    const fetchData = async (operation?: () => Promise<Paginated<T>>) => {
        if (config.interval && intervall.current === null) {
            dispatch({ type: "request" });
        } else if (!config.interval) {
            dispatch({ type: "request" });
        }

        try {
            let res: Paginated<T>;
            if (operation) {
                res = await operation();
            } else {
                res = await config.operation({ page: "0" });
            }
            dispatch({ type: "success", payload: res });
            return res;
        } catch (e) {
            dispatch({ type: "error", payload: e });
            return undefined;
        }
    };

    const refresh = async (operation?: typeof config.operation) => {
        dispatch({ type: "refresh" });
        try {
            let res: Paginated<T>;
            if (operation) {
                res = await operation({ page: "0" });
            } else {
                res = await config.operation({ page: "0" });
            }
            dispatch({ type: "success", payload: res });
            return res;
        } catch (e) {
            dispatch({ type: "error", payload: e });
            return undefined;
        }
    };

    const loadMore = async () => {
        if (state.data && state.data.currentPage < state.data.totalPages) {
            dispatch({ type: "loading more" });
            try {
                // we increment the fetch here.
                // we go inside else on initial fetch.
                const res: Paginated<T> = await config.operation({ page: (state.data.currentPage + 1).toString() });
                dispatch({ type: "loaded more", payload: res });
                return res;
            } catch (e) {
                dispatch({ type: "error", payload: e });
                return undefined;
            }
        }
    };

    const reset = () => {
        clearInterval(intervall.current);
        dispatch({ type: "reset" });
    };

    useEffect(() => {
        if (config.interval) {
            intervall.current = setInterval(async () => {
                fetchData().catch(() => clearInterval(intervall.current));
            }, config.interval);
            fetchData().catch(console.error);
        } else {
            fetchData().catch(console.error);
        }
        return () => clearInterval(intervall.current);
    }, config.deps ?? []);

    return { ...state, data: useList<T>(state.data), loadMore, fetchData, dispatch, reset, refresh };
}
