import { useCallback, useEffect, useState } from "react";
import { cloneDeep } from "lodash";
import { Paginated } from "../shared";
import Utils from "../statics/Utils";

export interface IUseList<T extends { id: any }, Identifier = T["id"]> extends Paginated<T> {
    // The real data inside the list.
    content: T[];
    // set new items to the list.
    updateItems: (data: Paginated<T>) => void;
    // the current selection (selected items)
    selection: boolean[];
    // master checkbox - select all.
    master: boolean;
    // select all items.
    selectAll: () => void;
    // move the element at the index to the start of the list.
    moveToStart: (index: number) => void;
    // move the element at the index to the end of the list.
    moveToEnd: (index: number) => void;
    // move an element to a specific index.
    moveToIndex: (elementAtIndex: number, toIndex: number) => void;
    // Remove all elements the callbackFn returns true for from the list.
    removeFromList: (compareFn: (value: T, index: number) => boolean) => void;
    // select an element by index.
    select: (index: number) => void;
    // execute a bulk operation. Executes the callbackFn. If an error occurs the handle error function gets called.
    // All currently selected elements are removed from the list.
    executeBulkOperation: (
        callbackFn: (ids: Identifier[]) => void,
        handleError?: (err: Error, items: T[]) => void,
    ) => void;
    // execute a single operation. see above
    executeOperation: (
        index: number,
        callbackFn: (id: Identifier) => void,
        handleError?: (err: Error, item: T) => void,
    ) => void;
    // add an item to the list.
    addToList: (item: T, compareFn?: (a: T, b: T) => number) => void;
    unshift: (...item: T[]) => void;
    push: (...item: T[]) => void;
    insertAt: (index: Identifier, ...items: T[]) => void;
    setItems: (items: T[]) => void;

    // remove an item from the list
    removeAt: (index: Identifier) => void;
    // sort the array by the provided callbackFn.
    sortBy: (compareFn: (a: T, b: T) => number) => void;
    update: (item: T, index: number) => void;
}

export function useList<T extends { id: any }, Identifier = T["id"]>(initial?: Paginated<T>): IUseList<T> {
    const indexEntries = (data: T[]) => {
        return data.map(() => false);
    };

    const calculateInitialState = useCallback((): Paginated<T> & {
        content: T[];
        selection: boolean[];
        master: boolean;
    } => {
        if (initial) {
            return {
                ...initial,
                selection: indexEntries(initial.content),
                master: false,
            };
        }
        return {
            content: [],
            selection: [],
            totalElements: 0,
            currentPage: 1,
            master: false,
            numberOfElements: 25,
            empty: true,
            totalPages: 0,
        };
    }, [initial]);

    const [raw, setRaw] = useState<Paginated<T> & { content: T[]; selection: boolean[]; master: boolean }>(
        calculateInitialState,
    );

    useEffect(() => {
        setRaw(calculateInitialState());
    }, [calculateInitialState, initial]);

    const selectAll = () => {
        setRaw({ ...raw, selection: raw.selection.map(bool => !bool), master: !raw.master });
    };

    // Add a new item to the list.
    // Provide a compare function to sort the array afterwards.
    const addToList = (item: T, compareFn?: (a: T, b: T) => number) => {
        let list = cloneDeep(raw.content);
        list.unshift(item);
        if (compareFn) {
            list = list.sort(compareFn);
        }
        setRaw({ ...raw, content: list });
    };

    const removeFromList = (callbackFn: (value: T, index: number, arr: T[]) => boolean) => {
        const list = cloneDeep(raw.content);
        const indices: number[] = [];
        const change = list.filter((value: T, index: number, arr: T[]) => {
            const shouldGetDeleted: boolean = callbackFn(value, index, arr);
            if (shouldGetDeleted) {
                // push the index in separate array.
                indices.push(index);
            }
            // we need to invert it because we use the filter function.
            return !shouldGetDeleted;
        });
        if (indices.length > 1) {
            // considering the selection set everything back to false.
            // We are doing a mass update here. After the update the master should be false.
            setRaw({
                ...raw,
                content: change,
                selection: indexEntries(change),
                master: false,
            });
            return;
        }
        // just a single update.
        // we need to update the selection here.
        const sel = [...raw.selection];
        sel.splice(indices[0], 1);
        setRaw({
            ...raw,
            content: change,
            selection: sel,
        });
    };

    const executeBulkOperation = (
        callbackFn: (ids: Identifier[]) => void,
        handleError?: (err: Error, items: T[]) => void,
    ) => {
        // get all the currently selected items.
        const selectedItems = raw.content.filter((item, index) => raw.selection[index]);
        // execute the operation.
        try {
            callbackFn(selectedItems.map(item => item.id));
        } catch (err) {
            handleError && handleError(err, selectedItems);
        }

        // remove all selected items from the list.
        removeFromList(item => selectedItems.some(i => i.id === item.id));
    };

    const unshift = (...items: T[]) => {
        setRaw(prevState => {
            return {
                ...prevState,
                items: items.concat(prevState.content),
            };
        });
    };

    const push = (...items: T[]) => {
        setRaw(prevState => {
            return {
                ...prevState,
                items: prevState.content.concat(items),
            };
        });
    };

    const executeOperation = (
        index: number,
        callbackFn: (id: Identifier) => void,
        handleError?: (err: Error, item: T) => void,
    ) => {
        const item: T = raw.content[index];
        try {
            callbackFn(item.id);
        } catch (err) {
            handleError && handleError(err, item);
        }

        removeFromList(i => i.id === item.id);
    };

    const moveToIndex = (elementAtIndex: number, toIndex: number) => {
        const list = cloneDeep(raw.content);
        setRaw({ ...raw, content: Utils.reorder(list, elementAtIndex, toIndex) });
    };

    const moveToStart = (index: number) => {
        const list = cloneDeep(raw.content);
        const [item] = list.splice(index, 1);
        list.unshift(item);
        setRaw({ ...raw, content: list });
    };

    const moveToEnd = (index: number) => {
        const list = cloneDeep(raw.content);
        const [item] = list.splice(index, 1);
        list.push(item);
        setRaw({ ...raw, content: list });
    };

    const sortBy = (compareFn: (a: T, b: T) => number) => {
        const list = cloneDeep(raw.content);
        setRaw({ ...raw, content: list.sort(compareFn) });
    };

    const select = (index: number) => {
        const sel = [...raw.selection];
        sel[index] = !sel[index];
        setRaw({ ...raw, selection: sel });
    };

    const updateItems = (data: Paginated<T>) => {
        setRaw({
            ...raw,
            content: data.content,
            selection: indexEntries(data.content),
            totalElements: data.totalElements,
            numberOfElements: data.numberOfElements,
            currentPage: data.currentPage,
        });
    };

    const setItems = (data: T[]) => {
        setRaw({
            ...raw,
            content: data,
            selection: indexEntries(data),
        });
    };

    const insertAt = (index: number, ...items: T[]) => {
        const list = cloneDeep(raw.content);
        list.splice(index, 0, ...items);
        setRaw({
            ...raw,
            content: list,
        });
    };

    const update = (item: T, index: number) => {
        const list = cloneDeep(raw.content);
        list[index] = item;
        setRaw({
            ...raw,
            content: list,
        });
    };

    const removeAt = (index: number) => {
        const list = cloneDeep(raw.content);
        list.splice(index, 1);
        setRaw({
            ...raw,
            content: list,
        });
    };

    return {
        ...raw,
        removeAt,
        insertAt,
        push,
        addToList,
        updateItems,
        removeFromList,
        select,
        selectAll,
        sortBy,
        executeOperation,
        executeBulkOperation,
        moveToStart,
        setItems,
        moveToEnd,
        moveToIndex,
        unshift,
        update,
    };
}
