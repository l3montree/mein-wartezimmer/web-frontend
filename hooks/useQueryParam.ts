import { useRouter } from "next/router";

export function useQueryParam(name: string): [string | null, (newValue: string | number) => void] {
    const { push, pathname } = useRouter();
    const params = new URLSearchParams(location.search);
    const updateParam = newValue => {
        params.set(name, newValue);
        push(`${pathname}?${params}`, `${location.pathname}?${params}`, { shallow: true });
    };

    const value: string | null = params.get(name);

    return [value, updateParam];
}
