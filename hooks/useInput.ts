import React, { useState } from "react";

export interface IUseInput<T> {
    onChange: (value: any) => void;
    value: T;
    error: boolean;
}

export function useInput<T>(config?: { initial?: T; validator?: (value: T) => boolean }): IUseInput<T> {
    const [value, setValue] = useState<T>(config?.initial || undefined);
    return {
        onChange: e => setValue(e.target.value),
        value,
        error: !(config && config.validator ? config.validator(value || undefined) : true),
    };
}
