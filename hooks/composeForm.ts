// first type parameter is the parameter of the setValue function, second is the value type.
// necessary due to multi selects and the different value compared to initial items type.
export interface FormElement<V> {
    // value of the form
    value: V;
    setValue: (value: V) => void;
}

export function composeForm<T>(formElements: { [key in keyof Partial<T>]: FormElement<T[key]> }) {
    const getValues = (): { [key in keyof Partial<T>]: T[key] } => {
        const values = {} as { [key in keyof Partial<T>]: T[key] };
        Object.keys(formElements).forEach(key => {
            // @ts-ignore
            values[key] = formElements[key].value;
        });
        return values;
    };

    // populates the form items with data. In most cases an interface is provided as data source
    // which is just prefixed by I<ClassName>
    // eg. the Form to create a new Discussion object might gets populated by the IDiscussion data-type
    // to verify this works correctly some types have to be changed. If the value at the given key is an array
    // we only extract the ids of the elements. This simplifies the communication with the server and reduces server-side logic
    const populate = (data: { [key in keyof Partial<T>]: T[key] | { id: number }[] }) => {
        Object.keys(formElements).forEach(key => {
            // @ts-ignore
            if (data[key] instanceof Array) {
                // we have to deal with an array here.
                // @ts-ignore
                formElements[key].setValue(data[key].map(object => object));
            } else {
                // just regular setting.
                // @ts-ignore
                formElements[key].setValue(data[key]);
            }
        });
    };

    return {
        getValues,
        populate,
    };
}
