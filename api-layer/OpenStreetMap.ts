import { PublicAPI } from "../statics/API";
import { OpenStreetMapResponse } from "../shared";

export default class OpenStreetMap {
    static async search(query: string): Promise<OpenStreetMapResponse[]> {
        return (
            await PublicAPI.get(
                `https://nominatim.openstreetmap.org/search/${query}?format=json&addressdetails=1&accept-language=de`,
            )
        ).data;
    }
}
