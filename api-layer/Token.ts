import { Token as TokenType } from "../shared/auth";

/**
 * Token response from auth route.
 */
export interface IToken {
    /**
     * Access token - used in header on every API call.
     * Short expiration time.
     */
    accessToken: string;
    /**
     * Refresh token - used to get a new access token.
     * Random string saved in database.
     * Long expiration time.
     */
    refreshToken: string;
}

/**
 * Class to provide functionality considering tokens
 * This includes storing and retrieving.
 */
export default class Token implements Omit<TokenType, "tokenType" | "expiresIn"> {
    /**
     * Get a new token object.
     * Retrieves the token from async storage.
     */
    public static get(): Token | false {
        const token = localStorage.getItem("accessToken");
        const refreshToken = localStorage.getItem("refreshToken");

        if (token && refreshToken) {
            return new Token(token, refreshToken);
        }
        return false;
    }

    /**
     * Destroys the refresh token and the access token in storage.
     */
    public static destroy() {
        localStorage.removeItem("accessToken");
        localStorage.removeItem("refreshToken");
    }

    /**
     * Get the refresh token.
     * Returns the refresh token or false | null if not found.
     */
    public static getRefreshToken(): string | false {
        return localStorage.getItem("refreshToken");
    }

    public constructor(public readonly accessToken: string, public readonly refreshToken: string) {}

    /**
     * Save token object in async storage.
     * TODO: Implement encryption before storing in AsyncStorage.
     */
    public save() {
        localStorage.setItem("accessToken", this.accessToken);
        localStorage.setItem("refreshToken", this.refreshToken);
    }
}
