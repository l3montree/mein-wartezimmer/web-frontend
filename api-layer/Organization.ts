import Networkable from "./abstract/Networkable";
import { OrganizationRoutes } from "../shared/routes";
import { PublicAPI } from "../statics/API";
import { Organization as SharedOrganization } from "../shared";
import { Section } from "../shared/auth";

export default class Organization extends Networkable<OrganizationRoutes> {
    protected url: string = "organization";

    static async search(query: string): Promise<Array<SharedOrganization & { __sections__: Section[] }>> {
        return (await PublicAPI.get(`organization/search?q=${encodeURI(query)}`)).data;
    }

    static async getBySlug(slug: string): Promise<SharedOrganization & { __sections__: Section[] }> {
        return (await PublicAPI.get(`organization/${slug}`)).data;
    }
}
