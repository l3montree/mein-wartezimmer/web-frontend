import API from "../../statics/API";
import { RouteMap } from "../../shared/routes";
import Application from "../../statics/Application";

export default abstract class Networkable<Map extends RouteMap<any>> {
    protected abstract url: string;

    public async show(param: Map["SHOW"]["request"]): Promise<Map["SHOW"]["response"]> {
        return (await API.get(`${this.url}/${param.id}`)).data;
    }

    public async store(param: Map["POST"]["request"]): Promise<Map["POST"]["response"]> {
        return (await API.post(this.url, param)).data;
    }

    public async update(param: Map["PUT"]["request"]): Promise<Map["PUT"]["response"]> {
        return (await API.put(`${this.url}/${param.id}`, param)).data;
    }

    public async patch(param: Map["PATCH"]["request"]): Promise<Map["PATCH"]["response"]> {
        return (await API.patch(`${this.url}/${param.id}`, param)).data;
    }

    public async filter(query?: URLSearchParams): Promise<Map["GET"]["response"]> {
        return (await API.get(`${this.url}?${query}`)).data;
    }

    public async destroy(param: Map["DELETE"]["request"]): Promise<Map["DELETE"]["response"]> {
        return (await API.delete(`${this.url}/${param.id}`)).data;
    }
}
