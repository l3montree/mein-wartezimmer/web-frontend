import Networkable from "./abstract/Networkable";
import { TicketRoutes } from "../shared/routes";
import { Organization, Ticket as SharedTicket } from "../shared";
import API from "../statics/API";
import { Section } from "../shared/auth";

export default class Ticket extends Networkable<TicketRoutes> {
    protected url: string = null;
    constructor(organizationId: number) {
        super();
        this.url = `organizations/${organizationId}/tickets`;
    }

    static async get(
        ticketId: string,
    ): Promise<SharedTicket & { time: number; position: number; organization: Organization; section: Section }> {
        return (await API.get(`tickets/${ticketId}`)).data;
    }

    public async storePublic(param: TicketRoutes["POST"]["request"]): Promise<TicketRoutes["POST"]["response"]> {
        return (await API.post(`public/${this.url}`, param)).data;
    }

    async moveAfter(ticketToMoveId: number, afterTicketId: number) {
        return (await API.patch(`${this.url}/${ticketToMoveId}/move-after/${afterTicketId}`)).data;
    }

    async moveBefore(ticketToMoveId: number, afterTicketId: number) {
        return (await API.patch(`${this.url}/${ticketToMoveId}/move-before/${afterTicketId}`)).data;
    }
}
