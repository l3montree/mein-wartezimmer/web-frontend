const withSass = require("@zeit/next-sass");
const withOffline = require("next-offline");
require("dotenv").config();

module.exports = withOffline(
    withSass({
        workboxOpts: {
            swDest: process.env.NEXT_EXPORT ? "service-worker.js" : "static/service-worker.js",
            runtimeCaching: [
                {
                    urlPattern: /^https?.*/,
                    handler: "NetworkFirst",
                    options: {
                        cacheName: "offlineCache",
                        expiration: {
                            maxEntries: 200,
                        },
                    },
                },
            ],
        },
        experimental: {
            async rewrites() {
                return [
                    {
                        source: "/service-worker.js",
                        destination: "/_next/static/service-worker.js",
                    },
                ];
            },
        },
        env: {
            API_HOST: process.env.API_HOST,
        },
    }),
);
