import React from "react";
import { IDispatchMethod } from "./index";
import { AuthRoutes } from "../shared/routes";
import { Organization, Ticket as SharedTicket } from "../shared";
import { Section } from "../shared/auth";
import Storage from "../statics/Storage";

/**
 * Global available variables.
 */
export interface IAppContext {
    organization: Organization & { sections: Section[] };
    tickets: SharedTicket[];
    activeTicket?: SharedTicket & {
        time: number;
        position: number;
        organization: Organization;
        section: Section;
        currentTime: Date;
    };
}

/**
 * "Empty" App context.
 */
export const defaultAppContext: IAppContext & IDispatchMethod<IAppContextActions> = {
    tickets: [],
    organization: {
        title: "",
        publicAccessible: false,
        id: 0,
        address: "",
        phoneNumber: "",
        averageTimeNeeded: 0,
        lat: "",
        lon: "",
        email: "",
        slug: "",
        sections: [],
    },
    dispatch: () => {
        throw new Error("Please provide a dispatch method for the App Context");
    },
};

export type IAppContextActions =
    | { type: "SUCCESSFUL_SYNC"; payload: AuthRoutes["SYNC"]["response"] }
    | { type: "LOGOUT" }
    | { type: "SET_TICKETS"; payload: SharedTicket[] }
    | { type: "SAVE_TICKET"; payload: SharedTicket }
    | { type: "CLEAR_TICKET" }
    | {
          type: "SET_TICKET";
          payload: SharedTicket & {
              time: number;
              position: number;
              organization: Organization;
              section: Section;
              currentTime: Date;
          };
      }
    | { type: "DELETE_TICKET"; payload: number };
export const AppContext = React.createContext(defaultAppContext);

export function AppReducer(state: IAppContext, action: IAppContextActions): IAppContext {
    switch (action.type) {
        case "SUCCESSFUL_SYNC": {
            return {
                ...state,
                organization: action.payload,
            };
        }
        case "DELETE_TICKET":
            const tickets = state.tickets.filter(ticket => ticket.id !== action.payload);
            Storage.saveLocalTickets(tickets);
            return {
                ...state,
                tickets,
            };
        case "SAVE_TICKET": {
            const tickets = [...state.tickets, action.payload];
            Storage.saveLocalTickets(tickets);
            return {
                ...state,
                tickets,
            };
        }
        case "SET_TICKETS":
            return {
                ...state,
                tickets: action.payload,
            };
        case "CLEAR_TICKET":
            return {
                ...state,
                activeTicket: undefined,
            };
        case "SET_TICKET":
            return {
                ...state,
                activeTicket: action.payload,
            };
        case "LOGOUT":
            return defaultAppContext;
        default:
            return state;
    }
}
