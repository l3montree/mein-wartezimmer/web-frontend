export interface IDispatchMethod<Actions> {
    dispatch: (action: Actions) => void;
}
